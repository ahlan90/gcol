from django import forms
from django.forms import ModelForm
from django_select2.forms import Select2Widget
from tempus_dominus.widgets import DateTimePicker

from .models import *
from cliente.models import Anotacao


class LancamentoForm(ModelForm):
    data = forms.DateTimeField(
        widget=DateTimePicker(
            attrs={
                'append': 'fa fa-calendar',
                'input_toggle': True,
                'icon_toggle': True,
            }
        )
    )

    class Meta:
        model = Lancamento
        exclude = [ 'pago', 'valor']
        widgets = {
            'abastecedor': forms.HiddenInput(),
            'lavoura': Select2Widget(),
            'operario': Select2Widget(),
            'unidade_medida': Select2Widget(),
        }

    def __init__(self, *args, cliente=None, **kwargs):
        super(LancamentoForm, self).__init__(*args, **kwargs)
        if cliente:
            self.fields['operario'].queryset = Operario.objects.filter(cliente_criacao=cliente).filter(inativo=False)
            self.fields['preco_unidade_medida'].queryset = PrecoUnidadeMedida.objects.filter(
                lavoura__in=Lavoura.objects.filter(cliente_criacao=cliente).filter(inativa=False))


class AnotacaoOperarioForm(ModelForm):

    data = forms.DateTimeField(
        widget=DateTimePicker(
            attrs={
                'append': 'fa fa-calendar',
                'input_toggle': True,
                'icon_toggle': True,
            }
        )
    )

    class Meta:
        model = Anotacao
        exclude = ['abastecedor']
        widgets = {
            'operario': Select2Widget,
            'usuario_criacao': forms.HiddenInput(),
            'texto': forms.Textarea(),
        }

    def __init__(self, *args, cliente=None, **kwargs):
        super(AnotacaoOperarioForm, self).__init__(*args, **kwargs)
        if cliente:
            self.fields['operario'].queryset = Operario.objects.filter(cliente_criacao=cliente).filter(inativo=False)


class PagamentoForm(ModelForm):
    class Meta:
        model = Lancamento
        fields = '__all__'
