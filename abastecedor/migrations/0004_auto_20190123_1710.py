# Generated by Django 2.1.4 on 2019-01-23 19:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('abastecedor', '0003_auto_20190122_2211'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='anotacaooperario',
            name='operario',
        ),
        migrations.RemoveField(
            model_name='anotacaooperario',
            name='usuario_criacao',
        ),
        migrations.DeleteModel(
            name='AnotacaoOperario',
        ),
    ]
