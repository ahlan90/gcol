# Generated by Django 2.1.4 on 2019-02-18 21:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('abastecedor', '0008_auto_20190204_1646'),
    ]

    operations = [
        migrations.AddField(
            model_name='lancamento',
            name='paga',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
