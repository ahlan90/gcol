# Generated by Django 2.1.4 on 2019-08-17 01:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('abastecedor', '0018_auto_20190321_2231'),
    ]

    operations = [
        migrations.CreateModel(
            name='Recibo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.DateTimeField(auto_now_add=True)),
                ('cancelado', models.BooleanField(default=False)),
            ],
        ),
        migrations.AlterModelOptions(
            name='lancamento',
            options={'ordering': ['-data']},
        ),
        migrations.AddField(
            model_name='lancamento',
            name='recibo',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='abastecedor.Recibo'),
        ),
    ]
