from django.contrib.auth.models import User
from django.db import models
from cliente.models import Cultivo, Lavoura, Operario, Abastecedor, PrecoUnidadeMedida
from gerenciamento.models import UnidadeMedida


class Recibo(models.Model):

    data = models.DateTimeField(auto_now_add=True)
    cancelado = models.BooleanField(default=False)
    valor_total = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)


class Lancamento(models.Model):

    data = models.DateTimeField(null=True, blank=True)
    operario = models.ForeignKey(Operario, on_delete=models.CASCADE)
    preco_unidade_medida = models.ForeignKey(PrecoUnidadeMedida, on_delete=models.CASCADE)
    credito = models.DecimalField(max_digits=10, decimal_places=2)

    abastecedor = models.ForeignKey(Abastecedor, on_delete=models.CASCADE, related_name='abatecedor_criacao')
    cancelada = models.BooleanField(default=False, null=True)
    motivo_cancelamento = models.CharField(max_length=3000, null=True, blank=True)
    pago = models.BooleanField(default=False)
    valor = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    recibo = models.ForeignKey(Recibo, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return 'Id: %s, Operario: %s,  Abastecedor: %s' % (self.pk, self.operario, self.abastecedor)

    def calcula_valor(self):
        self.valor = self.credito * self.preco_unidade_medida.preco

    def credito_unidade_total(self):
        return self.credito * self.preco_unidade_medida.unidade_medida.quantidade_unidade



    class Meta:
        ordering = ['-data']


