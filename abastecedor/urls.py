from django.urls import path, include
from . import views


urlpatterns = [
    path('dashboard/', views.dashboard, name='dashboard_abastecedor'),

    path('lancamento/criar/', views.lancamento_criar, name='lancamento_criar'),
    path('lancamento/listar/', views.lancamento_listar, name='lancamento_listar'),
    path('lancamento/atualizar/<int:lancamento_id>/', views.lancamento_atualizar, name='lancamento_atualizar'),
    path('lancamento/excluir/<int:lancamento_id>/', views.lancamento_excluir, name='lancamento_excluir'),

    path('anotacao/criar/', views.anotacao_operario_criar, name='anotacao_operario_criar'),
    path('anotacao/listar/', views.anotacao_operario_listar, name='anotacao_operario_listar'),
    path('anotacao/atualizar/<int:anotacao_id>/', views.anotacao_operario_atualizar, name='anotacao_operario_atualizar'),
    path('anotacao/excluir/<int:anotacao_id>/', views.anotacao_operario_excluir, name='anotacao_operario_excluir'),
]