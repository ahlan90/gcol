from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from .forms import *
from cliente.models import Lavoura, Operario, Abastecedor


@login_required
def dashboard(request, template_name='abastecedor/dashboard/dashboard.html'):

    return render(request, template_name, {'menu_dashboard_abastecedor': 'active'})


@login_required
def lancamento_criar(request, template_name='abastecedor/lancamento/lancamento_criar.html'):

    abastecedor = Abastecedor.objects.get(user=request.user)
    cliente = abastecedor.cliente_criacao

    if request.method == "POST":
        form = LancamentoForm(request.POST)
        if form.is_valid():
            lancamento = form.save(commit=False)
            lancamento.calcula_valor()
            lancamento.save()
            return redirect('lancamento_listar')
    else:
        form = LancamentoForm(initial={'abastecedor': request.user.usuario_abastecedor}, cliente=cliente)

    return render(request, template_name, {'form': form, 'menu_lancamento': 'active'})


@login_required
def lancamento_listar(request, template_name='abastecedor/lancamento/lancamento_lista.html'):

    lancamentos = Lancamento.objects.filter(abastecedor=request.user.usuario_abastecedor)

    data = {}

    data['lancamentos'] = lancamentos
    data['menu_lancamento'] = 'active'

    return render(request, template_name, data)


@login_required
def lancamento_atualizar(request, lancamento_id, template_name='abastecedor/lancamento/lancamento_atualizar.html'):

    lancamento = get_object_or_404(Lancamento, pk=lancamento_id)

    if request.method == 'POST':
        form = LancamentoForm(request.POST, instance=lancamento)
        if form.is_valid():
            lancamento = form.save(commit=False)
            lancamento.calcula_valor()
            lancamento.save()
            return redirect('lancamento_listar')
    else:
        form = LancamentoForm(instance=lancamento)

    return render(request, template_name,
                  {'form': form, 'menu_lancamento': 'active'})


@login_required
def lancamento_excluir(request, lancamento_id):

    lancamento = get_object_or_404(Lancamento, pk=lancamento_id)

    data = dict()

    if request.method == 'POST':
        lancamento.delete()
        data['form_is_valid'] = True
        lancamentos = Lancamento.objects.filter(abastecedor=request.user.usuario_abastecedor)
        data['html_lancamento_list'] = render_to_string('abastecedor/lancamento/lancamento_lista_parcial.html', {
            'lancamentos': lancamentos
        })
    else:
        context = {'lancamento': lancamento, 'menu_lancamento': 'active'}
        data['html_form'] = render_to_string('abastecedor/lancamento/lancamento_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)




@login_required
def anotacao_operario_criar(request, template_name='abastecedor/anotacao_operario/anotacao_criar.html'):

    abastecedor = Abastecedor.objects.get(user=request.user)
    cliente = abastecedor.cliente_criacao

    if request.method == "POST":
        form = AnotacaoOperarioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('anotacao_operario_listar')
    else:
        form = AnotacaoOperarioForm(initial={'usuario_criacao': request.user}, cliente=cliente)

    return render(request, template_name, {'form': form, 'menu_operario_anotacao': 'active'})


@login_required
def anotacao_operario_listar(request, template_name='abastecedor/anotacao_operario/anotacao_lista.html'):

    anotacoes = Anotacao.objects.filter(usuario_criacao=request.user).filter(abastecedor__isnull=True)

    data = {}

    data['anotacoes'] = anotacoes
    data['menu_operario_anotacao'] = 'active'

    return render(request, template_name, data)


@login_required
def anotacao_operario_atualizar(request, anotacao_id, template_name='abastecedor/anotacao_operario/anotacao_atualizar.html'):

    abastecedor = Abastecedor.objects.get(user=request.user)
    cliente = abastecedor.cliente_criacao

    anotacao = get_object_or_404(Anotacao, pk=anotacao_id)

    if request.method == 'POST':
        form = AnotacaoOperarioForm(request.POST, instance=anotacao)
        if form.is_valid():
            form.save()
            return redirect('anotacao_operario_listar')
    else:
        form = AnotacaoOperarioForm(instance=anotacao, cliente=cliente)

    return render(request, template_name,
                  {'form': form, 'menu_operario_anotacao': 'active'})


@login_required
def anotacao_operario_excluir(request, anotacao_id):

    anotacao = get_object_or_404(Anotacao, pk=anotacao_id)

    data = dict()

    if request.method == 'POST':
        anotacao.delete()
        data['form_is_valid'] = True
        anotacoes = Anotacao.objects.filter(usuario_criacao=request.user).filter(abastecedor__isnull=True)
        data['html_anotacao_list'] = render_to_string('abastecedor/anotacao_operario/anotacao_lista_parcial.html', {
            'anotacoes': anotacoes
        })
    else:
        context = {'anotacao': anotacao, 'menu_abastecedor_anotacao': 'active'}
        data['html_form'] = render_to_string('abastecedor/anotacao_operario/anotacao_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)
