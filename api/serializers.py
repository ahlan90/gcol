from rest_framework import serializers
from abastecedor.models import Lancamento
from cliente.models import Lavoura, Operario, PrecoUnidadeMedida, Anotacao
from gerenciamento.models import UnidadeMedida, Cultivo, Cliente


class UnidadeMedidaSerializer(serializers.ModelSerializer):
        class Meta:
            model = UnidadeMedida
            fields = '__all__'



class PrecoUnidadeMedidaSerializer(serializers.ModelSerializer):

    unidade_medida = UnidadeMedidaSerializer(read_only=True)

    class Meta:
        model = PrecoUnidadeMedida
        exclude = ['lavoura']



class CultivoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cultivo
        fields = '__all__'



class ClienteSerializer(serializers.ModelSerializer):

    nome_cliente = serializers.ReadOnlyField(source="__str__")

    class Meta:
        model = Cliente
        fields = ['id', 'nome_cliente']



class LavouraSerializer(serializers.ModelSerializer):

    unidades = serializers.SerializerMethodField('unidades_ativas')
    cultivo = CultivoSerializer(read_only=True)
    cliente_criacao = ClienteSerializer(read_only=True)

    def unidades_ativas(self, lavoura):
        qs = PrecoUnidadeMedida.objects.filter(unidade_medida__inativa=False).filter(lavoura=lavoura)
        serializer = PrecoUnidadeMedidaSerializer(instance=qs, many=True)
        return serializer.data

    class Meta:
        model = Lavoura
        fields = ('id', 'nome', 'hectares', 'cultivo', 'cliente_criacao', 'unidades')



class OperarioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Operario
        fields = ('id', 'nome', 'cpf', 'celular')



class LavouraTransacaoSerializer(serializers.ModelSerializer):

    lavoura = LavouraSerializer(read_only=True)

    class Meta:
        model = PrecoUnidadeMedida
        fields = '__all__'



class LancamentoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Lancamento
        fields = '__all__'



class LancamentoListaSerializer(serializers.ModelSerializer):

    preco_unidade_medida = LavouraTransacaoSerializer(read_only=True)
    operario = OperarioSerializer(read_only=True)

    class Meta:
        model = Lancamento
        fields = '__all__'


class AnotacaoListaSerializer(serializers.ModelSerializer):

    operario = OperarioSerializer(read_only=True)

    class Meta:
        model = Anotacao
        fields = '__all__'


class AnotacaoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Anotacao
        fields = '__all__'