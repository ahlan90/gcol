from django.urls import path, include

from api.views import AnotacaoOperarioViewSet
from .views import LavouraViewSet, OperarioViewSet, LancamentoViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'lavouras', LavouraViewSet, basename='lavouras')
router.register(r'operarios', OperarioViewSet, basename='operarios')
router.register(r'lancamentos', LancamentoViewSet, basename='lancamentos')
router.register(r'anotacoes', AnotacaoOperarioViewSet, basename='anotacoes')
urlpatterns = router.urls

urlpatterns = [
    path('', include(router.urls)),
]