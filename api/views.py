from rest_framework import viewsets
from api.serializers import LavouraSerializer, OperarioSerializer, LancamentoSerializer, LancamentoListaSerializer, \
    AnotacaoSerializer, AnotacaoListaSerializer
from abastecedor.forms import *
from cliente.models import Lavoura, Operario, Abastecedor
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import status


# Create your views here.
class LavouraViewSet(viewsets.ViewSet):

    permission_classes = [IsAuthenticated]

    def list(self, request):
        usuario = Abastecedor.objects.get(user=request.user)

        queryset = Lavoura.objects.filter(cliente_criacao=usuario.cliente_criacao).filter(inativa=False)
        serializer = LavouraSerializer(queryset, many=True)

        return Response(serializer.data)


class OperarioViewSet(viewsets.ViewSet):

    permission_classes = [IsAuthenticated]

    def list(self, request):
        usuario = Abastecedor.objects.get(user=request.user)

        queryset = Operario.objects.filter(cliente_criacao=usuario.cliente_criacao).filter(inativo=False)
        serializer = OperarioSerializer(queryset, many=True)

        return Response(serializer.data)


class LancamentoViewSet(viewsets.ViewSet):

    permission_classes = [IsAuthenticated]

    def list(self, request):

        usuario = Abastecedor.objects.get(user=request.user)
        operario_id = request.query_params.get('operario')

        if operario_id:
            queryset = Lancamento.objects.filter(abastecedor=usuario).filter(operario=operario_id).order_by('-id')
            serializer = LancamentoListaSerializer(queryset, many=True)
        else:
            queryset = Lancamento.objects.filter(abastecedor=usuario).order_by('-id')[:30]
            serializer = LancamentoListaSerializer(queryset, many=True)

        return Response(serializer.data)

    def create(self, request):

        serializer = LancamentoSerializer(data=request.data)

        if serializer.is_valid():

            lancamento = Lancamento.objects.filter(operario=serializer.initial_data['operario']).filter(data=serializer.initial_data['data'])

            if lancamento:
                serializer = LancamentoSerializer(lancamento[0])
            else:
                serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            print(str(serializer.errors))

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AnotacaoOperarioViewSet(viewsets.ViewSet):

    permission_classes = [IsAuthenticated]

    def list(self, request):
        queryset = Anotacao.objects.filter(usuario_criacao=request.user)
        serializer = AnotacaoListaSerializer(queryset, many=True)

        return Response(serializer.data)


    def create(self, request):

        request.data['usuario_criacao'] = request.user.id
        serializer = AnotacaoSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            print(str(serializer.errors))

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
