from django.contrib import admin 
from django.urls import path, include
from . import views

urlpatterns = [

    path('dashboard/', views.dashboard, name='dashboard_associado'),

    path('abastecedor/criar/', views.abastecedor_criar, name='abastecedor_criar_associado'),
    path('abastecedor/listar/', views.abastecedor_listar, name='abastecedor_listar_associado'),
    path('abastecedor/atualizar/<int:abastecedor_id>/', views.abastecedor_atualizar, name='abastecedor_atualizar_associado'),
    path('abastecedor/excluir/<int:abastecedor_id>/', views.abastecedor_excluir, name='abastecedor_excluir_associado'),

    path('anotacao/criar/', views.anotacao_abastecedor_criar, name='anotacao_abastecedor_criar_associado'),
    path('anotacao/listar/', views.anotacao_abastecedor_listar, name='anotacao_abastecedor_listar_associado'),
    path('anotacao/atualizar/<int:anotacao_id>/', views.anotacao_abastecedor_atualizar, name='anotacao_abastecedor_atualizar_associado'),
    path('anotacao/excluir/<int:anotacao_id>/', views.anotacao_abastecedor_excluir, name='anotacao_abastecedor_excluir_associado'),

    path('lancamento/', views.lancamento_cliente_listar, name='lancamento_cliente_listar_associado'),

    path('lavoura/criar/', views.lavoura_criar, name='lavoura_criar_associado'),
    path('lavoura/listar/', views.lavoura_listar, name='lavoura_listar_associado'),
    path('lavoura/atualizar/<int:lavoura_id>/', views.lavoura_atualizar, name='lavoura_atualizar_associado'),
    path('lavoura/excluir/<int:lavoura_id>/', views.lavoura_excluir, name='lavoura_excluir_associado'),

    path('operario/criar/', views.operario_criar, name='operario_criar_associado'),
    path('operario/listar/', views.operario_listar, name='operario_listar_associado'),
    path('operario/atualizar/<int:operario_id>/', views.operario_atualizar, name='operario_atualizar_associado'),
    path('operario/excluir/<int:operario_id>/', views.operario_excluir, name='operario_excluir_associado'),
    path('operario/qrcode/<int:operario_id>/', views.operario_qrcode, name='operario_qrcode_associado'),

    path('usuario/criar/', views.usuario_criar, name='usuario_criar_associado'),
    path('usuario/listar/', views.usuario_listar, name='usuario_listar_associado'),
    path('usuario/atualizar/<int:usuario_id>/', views.usuario_atualizar, name='usuario_atualizar_associado'),
    path('usuario/excluir/<int:usuario_id>/', views.usuario_excluir, name='usuario_excluir_associado'),
]