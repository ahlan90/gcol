from django.forms import inlineformset_factory
from cliente.forms import *
from core.forms import SignUpForm, SignUpUpdateForm
from abastecedor.forms import *
from gerenciamento.forms import ClienteForm, EnderecoClienteForm
from abastecedor.models import *
from gerenciamento.models import EnderecoResidencial
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


@login_required
def dashboard(request, template_name='cliente/dashboard/dashboard.html'):
    return render(request, template_name, {'menu_dashboard_cliente': 'active'})


@login_required
def abastecedor_criar(request, template_name='cliente/abastecedor/abastecedor_criar.html'):
    if request.method == "POST":
        form_signup = SignUpForm(request.POST)
        form = AbastecedorForm(request.POST)
        form_contato = ContatoForm(request.POST)
        form_endereco = EnderecoAbastecedorForm(request.POST)
        if form_signup.is_valid() and form.is_valid() and form_endereco.is_valid() and form_contato.is_valid():
            user = form_signup.save(commit=False)
            user.save()

            endereco = form_endereco.save(commit=False)
            endereco.save()

            contato = form_contato.save(commit=False)
            contato.save()

            abastecedor = form.save(commit=False)
            abastecedor.contato_proximo = contato
            abastecedor.endereco_residencial = endereco
            abastecedor.user = user
            abastecedor.cliente_criacao = request.user.usuario_cliente
            abastecedor.save()

            return redirect('abastecedor_listar')
    else:
        form_signup = SignUpForm()
        form = AbastecedorForm()
        form_endereco = EnderecoAbastecedorForm()
        form_contato = ContatoForm()

    return render(request, template_name, {'form_signup': form_signup, 'form': form, 'form_endereco': form_endereco,
                                           'form_contato': form_contato, 'menu_abastecedor': 'active'})


@login_required
def abastecedor_listar(request, template_name='cliente/abastecedor/abastecedor_lista.html'):

    cliente_associado = request.user.associado.cliente_associado

    abastecedores = Abastecedor.objects.filter(cliente_criacao=cliente_associado)

    data = {}
    data['abastecedors'] = abastecedores
    data['menu_abastecedor'] = 'active'
    return render(request, template_name, data)


@login_required
def abastecedor_atualizar(request, abastecedor_id, template_name='cliente/abastecedor/abastecedor_atualizar.html'):
    abastecedor = get_object_or_404(Abastecedor, pk=abastecedor_id)

    if request.method == 'POST':
        form_signup = SignUpUpdateForm(request.POST, instance=abastecedor.user)
        form = AbastecedorForm(request.POST, instance=abastecedor)
        form_contato = ContatoForm(request.POST, instance=abastecedor.contato_proximo)
        form_endereco = EnderecoAbastecedorForm(request.POST, instance=abastecedor.endereco_residencial)
        if form.is_valid() and form_endereco.is_valid() and form_contato.is_valid():
            endereco = form_endereco.save(commit=False)
            endereco.save()

            contato = form_contato.save(commit=False)
            contato.save()

            abastecedor = form.save(commit=False)
            abastecedor.contato_proximo = contato
            abastecedor.endereco_residencial = endereco

            abastecedor.save()
            return redirect('abastecedor_listar')
    else:
        form_signup = SignUpUpdateForm(instance=abastecedor.user)
        form = AbastecedorForm(instance=abastecedor)
        form_contato = ContatoForm(instance=abastecedor.contato_proximo)
        form_endereco = EnderecoAbastecedorForm(instance=abastecedor.endereco_residencial)

    return render(request, template_name, {'form_signup': form_signup, 'form': form, 'form_endereco': form_endereco,
                                           'form_contato': form_contato, 'menu_abastecedor': 'active'})


@login_required
def abastecedor_excluir(request, abastecedor_id):
    abastecedor = get_object_or_404(Abastecedor, pk=abastecedor_id)
    data = dict()
    if request.method == 'POST':
        abastecedor.delete()
        data['form_is_valid'] = True
        abastecedors = Abastecedor.objects.all()
        data['html_abastecedor_list'] = render_to_string('cliente/abastecedor/abastecedor_lista_parcial.html', {
            'abastecedors': abastecedors
        })
    else:
        context = {'abastecedor': abastecedor}
        data['html_form'] = render_to_string('cliente/abastecedor/abastecedor_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def anotacao_abastecedor_criar(request, template_name='cliente/anotacao/anotacao_criar.html'):
    if request.method == "POST":
        form = AnotacaoAbastecedorForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('anotacao_cliente_listar')
    else:
        form = AnotacaoAbastecedorForm(initial={'usuario_criacao': request.user})

    return render(request, template_name, {'form': form, 'menu_cliente_anotacao': 'active'})



@login_required
def anotacao_abastecedor_listar(request, template_name='cliente/anotacao/anotacao_lista.html'):
    anotacoes = Anotacao.objects.filter(usuario_criacao=request.user).filter(operario__isnull=True)
    data = {}
    data['anotacoes'] = anotacoes
    data['menu_cliente_anotacao'] = 'active'

    return render(request, template_name, data)


@login_required
def anotacao_abastecedor_atualizar(request, anotacao_id, template_name='cliente/anotacao/anotacao_atualizar.html'):
    anotacao = get_object_or_404(Anotacao, pk=anotacao_id)

    if request.method == 'POST':
        form = AnotacaoAbastecedorForm(request.POST, instance=anotacao)
        if form.is_valid():
            form.save()
            return redirect('anotacao_abastecedor_listar')
    else:
        form = AnotacaoAbastecedorForm(instance=anotacao)

    return render(request, template_name,
                  {'form': form, 'menu_cliente_anotacao': 'active'})


@login_required
def anotacao_abastecedor_excluir(request, anotacao_id):
    anotacao = get_object_or_404(Anotacao, pk=anotacao_id)
    data = dict()
    if request.method == 'POST':
        anotacao.delete()
        data['form_is_valid'] = True
        anotacoes = Anotacao.objects.all()
        data['html_anotacao_list'] = render_to_string('cliente/anotacao/anotacao_lista_parcial.html', {
            'anotacoes': anotacoes
        })
    else:
        context = {'anotacao': anotacao, 'menu_cliente_anotacao': 'active'}
        data['html_form'] = render_to_string('cliente/anotacao/anotacao_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


""" Atualização do perfil do cliente 
"""
@login_required
def perfil_atualizar(request, template_name='cliente/perfil/perfil_atualizar.html'):
    print('Teste')

    usuario = get_object_or_404(User, pk=request.user.id)
    print('Teste2')
    cliente = Cliente.objects.get(get)
    print('Teste3')
    if cliente.endereco != None:
        endereco = EnderecoResidencial.objects.get(pk=cliente.endereco.id)
    else:
        endereco = EnderecoResidencial()

    if request.method == 'POST':
        form = ClienteForm(request.POST, instance=cliente)
        form_user = AssociadoForm(request.POST, instance=usuario)
        form_endereco = EnderecoClienteForm(request.POST, instance=endereco)
        if form.is_valid() and form_endereco.is_valid() and form_user.is_valid():
            form_user.save()

            endereco_cliente = form_endereco.save(commit=False)
            endereco_cliente.save()

            cliente = form.save(commit=False)
            cliente.endereco = endereco_cliente
            cliente.save()

            return redirect('cliente_listar')
    else:
        form = ClienteForm(instance=cliente)
        form_user = AssociadoForm(instance=usuario)
        form_endereco = EnderecoClienteForm(instance=endereco)

    return render(request, template_name,
                  {'form': form, 'form_endereco': form_endereco, 'form_user': form_user, 'menu_cliente': 'active'})


@login_required
def lavoura_criar(request, template_name='cliente/lavoura/lavoura_criar.html'):
    if request.method == "POST":
        form = LavouraForm(request.POST)
        form_endereco = EnderecoLavouraForm(request.POST)
        if form.is_valid():
            lavoura = form.save(commit=False)
            lavoura.cliente_criacao = request.user.usuario_cliente
            lavoura.save()

            return redirect('lavoura_atualizar', lavoura_id=lavoura.pk)
    else:
        form = LavouraForm()

    return render(request, template_name, {'form': form, 'menu_lavoura': 'active'})



@login_required
def lavoura_listar(request, template_name='cliente/lavoura/lavoura_lista.html'):
    lavouras = Lavoura.objects.filter(cliente_criacao=request.user.associado.cliente_associado)
    data = {}
    data['lavouras'] = lavouras
    data['menu_lavoura'] = 'active'

    return render(request, template_name, data)


@login_required
def lavoura_atualizar(request, lavoura_id, template_name='cliente/lavoura/lavoura_atualizar.html'):
    lavoura = Lavoura.objects.get(pk=lavoura_id)

    PrecoFormSet = inlineformset_factory(Lavoura, PrecoUnidadeMedida, extra=1, fields='__all__')

    if request.method == "POST":
        form = LavouraForm(request.POST, instance=lavoura)
        form_endereco = EnderecoLavouraForm(request.POST, instance=lavoura.endereco)
        formset = PrecoFormSet(request.POST, instance=lavoura)
        if form.is_valid() and form_endereco.is_valid() and formset.is_valid():
            lavoura = form.save(commit=False)
            lavoura.cliente_criacao = request.user.usuario_cliente
            lavoura.save()
            endereco_lavoura = form_endereco.save(commit=False)
            endereco_lavoura.lavoura = lavoura
            endereco_lavoura.save()
            formset.save()
            return redirect('lavoura_listar')
    else:
        form = LavouraForm(instance=lavoura)
        form_endereco = EnderecoLavouraForm(instance=lavoura.endereco)
        formset = PrecoFormSet(instance=lavoura)

    return render(request, template_name,
                  {'form': form, 'form_endereco': form_endereco, 'formset': formset, 'menu_lavoura': 'active'})


@login_required
def lavoura_excluir(request, lavoura_id):
    lavoura = get_object_or_404(Lavoura, pk=lavoura_id)
    data = dict()
    if request.method == 'POST':
        lavoura.delete()
        data['form_is_valid'] = True
        lavouras = Lavoura.objects.all()
        data['html_lavoura_list'] = render_to_string('cliente/lavoura/recibo_lista_parcial.html', {
            'lavouras': lavouras
        })
    else:
        context = {'lavoura': lavoura}
        data['html_form'] = render_to_string('cliente/lavoura/lavoura_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def operario_criar(request, template_name='cliente/operario/operario_criar.html'):
    if request.method == "POST":
        form = OperarioForm(request.POST)
        form_contato = ContatoForm(request.POST)
        form_endereco = EnderecoOperarioForm(request.POST)
        if form.is_valid() and form_endereco.is_valid() and form_contato.is_valid():
            endereco = form_endereco.save(commit=False)
            endereco.save()

            contato = form_contato.save(commit=False)
            contato.save()

            operario = form.save(commit=False)
            operario.contato_proximo = contato
            operario.endereco_residencial = endereco
            operario.cliente_criacao = request.user.usuario_cliente

            operario.save()

            return redirect('operario_listar')
    else:
        form = OperarioForm(initial={'cliente': request.user.usuario_cliente})
        form_endereco = EnderecoOperarioForm()
        form_contato = ContatoForm(request.POST)

    return render(request, template_name, {'form': form, 'form_endereco': form_endereco, 'form_contato': form_contato,
                                           'menu_operario': 'active'})


@login_required
def operario_listar(request, template_name='cliente/operario/operario_lista.html'):

    cliente_associado = request.user.associado.cliente_associado

    operarios = Operario.objects.filter(cliente_criacao=cliente_associado)
    data = {}
    data['operarios'] = operarios
    data['menu_operario'] = 'active'
    data['teste'] = '1-1'

    return render(request, template_name, data)


@login_required
def operario_atualizar(request, operario_id, template_name='cliente/operario/operario_atualizar.html'):
    operario = get_object_or_404(Operario, pk=operario_id)

    if request.method == 'POST':
        form = OperarioForm(request.POST, instance=operario)
        form_contato = ContatoForm(request.POST, instance=operario.contato_proximo)
        form_endereco = EnderecoOperarioForm(request.POST, instance=operario.endereco_residencial)
        if form.is_valid() and form_endereco.is_valid() and form_contato.is_valid():
            endereco = form_endereco.save(commit=False)
            endereco.save()

            contato = form_contato.save(commit=False)
            contato.save()

            operario = form.save(commit=False)
            operario.contato_proximo = contato
            operario.endereco_residencial = endereco

            operario.save()

            return redirect('operario_listar')
    else:
        form = OperarioForm(instance=operario)
        form_contato = ContatoForm(instance=operario.contato_proximo)
        form_endereco = EnderecoOperarioForm(instance=operario.endereco_residencial)

    return render(request, template_name, {'form': form, 'form_endereco': form_endereco, 'form_contato': form_contato,
                                           'menu_operario': 'active'})


@login_required
def operario_excluir(request, operario_id):
    operario = get_object_or_404(Operario, pk=operario_id)
    data = dict()
    if request.method == 'POST':
        operario.delete()
        data['form_is_valid'] = True
        operarios = Operario.objects.all()
        data['html_operario_list'] = render_to_string('cliente/operario/operario_lista_parcial.html', {
            'operarios': operarios
        })
    else:
        context = {'operario': operario}
        data['html_form'] = render_to_string('cliente/operario/operario_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def operario_qrcode(request, operario_id):
    operario = get_object_or_404(Operario, pk=operario_id)
    data = dict()

    qrcode = str(operario.cliente_criacao.id) + '-' + str(operario.id)

    print('QRCode: ' + qrcode)

    context = {'qrcode': qrcode}
    data['html_form'] = render_to_string('cliente/operario/operario_qrcode.html', context, request=request)

    return JsonResponse(data)


@login_required
def usuario_criar(request, template_name='cliente/usuario/usuario_criar.html'):
    if request.method == "POST":
        form = AssociadoForm(request.POST)
        form_signup = SignUpForm(request.POST)
        if form.is_valid() and form_signup.is_valid():
            user = form_signup.save(commit=False)
            user.save()

            usuario = form.save(commit=False)
            usuario.user = user

            usuario.save()
            form.save_m2m()
            return redirect('usuario_listar')
    else:
        form = AssociadoForm()
        form_signup = SignUpForm(request.POST)

    return render(request, template_name, {'form': form, 'form_signup': form_signup, 'menu_usuario': 'active'})


@login_required
def usuario_listar(request, template_name='cliente/usuario/usuario_lista.html'):
    usuarios = Associado.objects.all();
    data = {}
    data['usuarios'] = usuarios
    data['menu_usuario'] = 'active'

    return render(request, template_name, data)


@login_required
def usuario_atualizar(request, usuario_id, template_name='cliente/usuario/usuario_atualizar.html'):
    usuario = get_object_or_404(Associado, pk=usuario_id)

    if request.method == 'POST':

        form = AssociadoForm(request.POST, instance=usuario)
        form_signup = SignUpUpdateForm(request.POST, instance=usuario.user)
        if form.is_valid() and form_signup.is_valid():
            user = form_signup.save(commit=False)
            user.save()

            usuario = form.save(commit=False)
            usuario.user = user

            usuario.save()
            form.save_m2m()
            return redirect('usuario_listar')
    else:
        form = AssociadoForm(instance=usuario)
        form_signup = SignUpUpdateForm(instance=usuario.user)

    return render(request, template_name, {'form': form, 'form_signup': form_signup, 'menu_usuario': 'active'})


@login_required
def usuario_excluir(request, usuario_id):
    usuario = get_object_or_404(Associado, pk=usuario_id)
    data = dict()
    if request.method == 'POST':
        usuario.delete()
        data['form_is_valid'] = True
        usuarios = Associado.objects.all()
        data['html_usuario_list'] = render_to_string('cliente/usuario/usuario_lista_parcial.html', {
            'usuarios': usuarios
        })
    else:
        context = {'usuario': usuario}
        data['html_form'] = render_to_string('cliente/usuario/usuario_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def lancamento_cliente_listar(request, template_name='cliente/lancamento/lancamento_lista.html'):

    cliente_associado = request.user.associado.cliente_associado

    abastecedores = Abastecedor.objects.filter(cliente_criacao=cliente_associado)

    lancamentos = Lancamento.objects.filter(abastecedor__in=abastecedores)


    data = {}

    data['lancamentos'] = lancamentos
    data['menu_cliente_lancamento'] = 'active'

    return render(request, template_name, data)