from django.contrib import admin
from .models import *

admin.site.register(Lavoura)
admin.site.register(EnderecoLavoura)
admin.site.register(EnderecoResidencial)
admin.site.register(ContatoProximo)
admin.site.register(Abastecedor)
admin.site.register(Operario)
admin.site.register(Associado)
admin.site.register(Anotacao)