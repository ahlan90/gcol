import django_filters
from django_filters.fields import ModelMultipleChoiceField
from django_filters.widgets import RangeWidget

from abastecedor.models import Lancamento
from cliente.models import Lavoura
from django_select2.forms import Select2MultipleWidget, Select2Widget


class LancamentoFilter(django_filters.FilterSet):

    data = django_filters.DateFromToRangeFilter(widget=RangeWidget(attrs={'type': "date", 'class': 'form-control'}))
    preco_unidade_medida__lavoura = ModelMultipleChoiceField(queryset=Lavoura.objects.all())

    class Meta:
        model = Lancamento
        fields = ['data', 'preco_unidade_medida__lavoura', 'preco_unidade_medida__unidade_medida', 'preco_unidade_medida__preco']

    def __init__(self, *args, cliente=None, **kwargs):
        super(LancamentoFilter, self).__init__(*args, **kwargs)
        if cliente:
            self.form.fields['preco_unidade_medida__lavoura'].queryset = Lavoura.objects.filter(cliente_criacao=cliente).filter(inativa=False)