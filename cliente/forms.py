from django import forms
from django.forms import ModelForm
from tempus_dominus.widgets import DateTimePicker

from abastecedor.models import Lancamento
from .models import *
from django_select2.forms import Select2MultipleWidget, Select2Widget


class LavouraForm(ModelForm):

    class Meta:
        model = Lavoura
        exclude = ['clientes', 'cliente_criacao']
        widgets = {
            'associados': Select2MultipleWidget,
            'agricultura': Select2Widget,
        }

    def __init__(self, *args, **kwargs):
        super(LavouraForm, self).__init__(*args, **kwargs)
        self.fields['cultivo'].queryset = Cultivo.objects.filter(inativo=False)


class EnderecoLavouraForm(ModelForm):
  
    class Meta:
        model = EnderecoLavoura
        exclude = ['lavoura']


class AbastecedorForm(ModelForm):
  
    class Meta:
        model = Abastecedor
        exclude = ['contato_proximo', 'endereco_residencial', 'user', 'cliente_criacao']


class AssociadoForm(ModelForm):

    class Meta:
        model = Associado
        exclude = ['user']
        widgets = {
            'lavouras': Select2MultipleWidget,
            'cliente_associado': forms.HiddenInput()
        }

    def __init__(self, *args, cliente=None, **kwargs):
        super(AssociadoForm, self).__init__(*args, **kwargs)
        if cliente:
            self.fields['lavouras'].queryset = Lavoura.objects.filter(cliente_criacao=cliente).filter(inativa=False)



class OperarioForm(ModelForm):
  
    class Meta:
        model = Operario
        exclude = ['contato_proximo', 'endereco_residencial', 'cliente_criacao', 'codigo_qrcode']


class EnderecoAbastecedorForm(ModelForm):
  
    class Meta:
        model = EnderecoResidencial
        exclude = ['abastecedor']


class EnderecoOperarioForm(ModelForm):
  
    class Meta:
        model = EnderecoResidencial
        exclude = ['abastecedor']


class ContatoForm(ModelForm):
  
    class Meta:
        model = ContatoProximo
        exclude = ['abastecedor']


class AnotacaoAbastecedorForm(ModelForm):

    data = forms.DateTimeField(
        widget=DateTimePicker(
            attrs={
                'append': 'fa fa-calendar',
                'input_toggle': True,
                'icon_toggle': True,
            }
        )
    )

    class Meta:
        model = Anotacao
        exclude = ['operario']
        widgets = {
            'abastecedor': Select2Widget,
            'usuario_criacao': forms.HiddenInput(),
            'texto': forms.Textarea(),
        }

    def __init__(self, *args, cliente=None, **kwargs):
        super(AnotacaoAbastecedorForm, self).__init__(*args, **kwargs)
        if cliente:
            self.fields['abastecedor'].queryset = Abastecedor.objects.filter(cliente_criacao=cliente).filter(inativo=False)



class PrecoUnidadeMedidaForm(ModelForm):
    class Meta:
        model = PrecoUnidadeMedida
        exclude = ['lavoura']

    def __init__(self, *args, **kwargs):
        super(PrecoUnidadeMedidaForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control textinput textInput'


class LancamentoCancelamentoForm(ModelForm):

    class Meta:
        model = Lancamento
        fields = ['motivo_cancelamento']


class PerfilForm(ModelForm):
    first_name = forms.CharField(max_length=30, label='Nome')
    last_name = forms.CharField(max_length=30, label='Sobrenome')
    email = forms.EmailField(max_length=254)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']


class LancamentoClienteForm(ModelForm):

    data = forms.DateTimeField(
        widget=DateTimePicker(
            attrs={
                'append': 'fa fa-calendar',
                'input_toggle': True,
                'icon_toggle': True,
            }
        )
    )

    class Meta:
        model = Lancamento
        exclude = ['valor',]
        widgets = {
            'lavoura': Select2Widget(),
            'operario': Select2Widget(),
            'unidade_medida': Select2Widget(),
        }

    def __init__(self, *args, cliente=None, **kwargs):
        super(LancamentoClienteForm, self).__init__(*args, **kwargs)
        if cliente:
            self.fields['operario'].queryset = Operario.objects.filter(cliente_criacao=cliente).filter(inativo=False)
            self.fields['preco_unidade_medida'].queryset = PrecoUnidadeMedida.objects.filter(
                lavoura__in=Lavoura.objects.filter(cliente_criacao=cliente).filter(inativa=False))


class DocumentoForm(ModelForm):

    class Meta:
        model = Documento
        exclude = ['usuario_criacao']
