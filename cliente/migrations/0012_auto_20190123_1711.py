# Generated by Django 2.1.4 on 2019-01-23 19:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0011_auto_20190123_1710'),
    ]

    operations = [
        migrations.RenameField(
            model_name='anotacao',
            old_name='descricao',
            new_name='texto',
        ),
    ]
