# Generated by Django 2.1.4 on 2019-02-03 14:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0016_auto_20190201_1804'),
    ]

    operations = [
        migrations.AlterField(
            model_name='abastecedor',
            name='cliente_criacao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cliente_criacao', to='gerenciamento.Cliente'),
        ),
        migrations.AlterField(
            model_name='lavoura',
            name='cliente_criacao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cliente_lavoura', to='gerenciamento.Cliente'),
        ),
        migrations.AlterField(
            model_name='lavoura',
            name='clientes',
            field=models.ManyToManyField(related_name='clientes_lavoura', to='gerenciamento.Cliente'),
        ),
        migrations.AlterField(
            model_name='operario',
            name='cliente_criacao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gerenciamento.Cliente'),
        ),
    ]
