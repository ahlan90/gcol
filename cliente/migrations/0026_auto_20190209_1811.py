# Generated by Django 2.1.4 on 2019-02-09 20:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0025_auto_20190209_1810'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lavoura',
            name='usuarios',
            field=models.ManyToManyField(blank=True, related_name='usuarios_lavoura', to='gerenciamento.Cliente'),
        ),
    ]
