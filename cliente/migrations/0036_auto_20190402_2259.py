# Generated by Django 2.1.4 on 2019-04-03 01:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0035_anotacao_arquivada'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operario',
            name='cpf',
            field=models.CharField(max_length=11),
        ),
    ]
