# Generated by Django 2.1.4 on 2019-08-17 12:19

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0039_documento'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documento',
            name='usuario_criacao',
            field=models.ForeignKey(auto_created=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
