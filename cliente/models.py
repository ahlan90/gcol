from django.db import models
from django.contrib.auth.models import User
from gerenciamento.models import UnidadeMedida, Cultivo, Cliente
from ckeditor.fields import RichTextField


class Lavoura(models.Model):

    nome = models.CharField(max_length=200)
    hectares = models.DecimalField(max_digits=6, decimal_places=2)
    cultivo = models.ForeignKey(Cultivo, on_delete=models.CASCADE)
    cliente_criacao = models.ForeignKey(Cliente, on_delete=models.CASCADE, related_name='cliente_lavoura')
    associados = models.ManyToManyField(Cliente, related_name='associados_lavoura', blank=True)

    inativa = models.BooleanField(default=False)

    def __str__(self):
        return '%s' % (self.nome)



class PrecoUnidadeMedida(models.Model):

    unidade_medida = models.ForeignKey(UnidadeMedida, on_delete=models.CASCADE)
    preco = models.DecimalField(max_digits=6, decimal_places=2)
    lavoura = models.ForeignKey(Lavoura, on_delete=models.CASCADE, related_name='unidades')

    def __str__(self):
        return '%s: %s - R$ %s' % (self.lavoura, self.unidade_medida, self.preco)


class EnderecoLavoura(models.Model):

    lavoura = models.OneToOneField(Lavoura, on_delete=models.CASCADE, primary_key=True, related_name='endereco')
    pais = models.CharField(max_length=200, blank=True)
    estado = models.CharField(max_length=200, blank=True)
    cidade = models.CharField(max_length=200, blank=True)
    cep = models.CharField(max_length=200, blank=True)
    corrego = models.CharField(max_length=200, blank=True)



class EnderecoResidencial(models.Model):

    pais = models.CharField(max_length=200, default='Brasil', blank=True)
    estado = models.CharField(max_length=200, default='Espírito Santo', blank=True)
    cidade = models.CharField(max_length=200, blank=True)
    cep = models.CharField(max_length=200, blank=True)
    bairro = models.CharField(max_length=200, blank=True)
    logradouro = models.CharField(max_length=200, blank=True)
    numero = models.CharField(max_length=200, blank=True)
    complemento = models.CharField(max_length=200, blank=True)



class ContatoProximo(models.Model):

    nome_contato = models.CharField('Nome',max_length=200, blank=True)
    celular_contato = models.CharField('Celular',max_length=200, blank=True)
    parentesco = models.CharField(max_length=200, blank=True)



class Abastecedor(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="usuario_abastecedor")

    cpf = models.CharField(max_length=200)
    celular = models.CharField(max_length=200)
    identidade = models.CharField(max_length=200, blank=True)
    carteira = models.CharField(max_length=200, blank=True)

    contato_proximo = models.OneToOneField(ContatoProximo, on_delete=models.CASCADE)
    endereco_residencial = models.OneToOneField(EnderecoResidencial, on_delete=models.CASCADE)

    cliente_criacao = models.ForeignKey(Cliente, on_delete=models.CASCADE, related_name="cliente_criacao")

    inativo = models.BooleanField(default=False)

    def __str__(self):
        return '%s - %s' % (self.pk, self.user.username)



class Operario(models.Model):

    nome = models.CharField(max_length=200)
    cpf = models.CharField(max_length=11)
    celular = models.CharField(max_length=200, blank=True)
    identidade = models.CharField(max_length=200, blank=True)
    carteira = models.CharField(max_length=200, blank=True)
    codigo_qrcode = models.CharField(max_length=200, blank=True)

    contato_proximo = models.OneToOneField(ContatoProximo, on_delete=models.CASCADE)
    endereco_residencial = models.OneToOneField(EnderecoResidencial, on_delete=models.CASCADE)
    cliente_criacao = models.ForeignKey(Cliente, on_delete=models.CASCADE)

    inativo = models.BooleanField(default=False)

    def __str__(self):
        return '%s' % (self.nome)



class Associado(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="associado")

    telefone = models.CharField(max_length=200)
    admin = models.BooleanField()
    lavouras = models.ManyToManyField(Lavoura, blank=True)

    cliente_associado = models.ForeignKey(Cliente, on_delete=models.CASCADE, related_name="cliente_associado")

    def __str__(self):
        return '%s' % (self.user.get_full_name)



class Anotacao(models.Model):

    data = models.DateTimeField(null=True, blank=True)
    usuario_criacao = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_anotacao")
    abastecedor = models.ForeignKey(Abastecedor, on_delete=models.CASCADE, null=True)
    operario = models.ForeignKey(Operario, on_delete=models.CASCADE, null=True)
    texto = models.CharField(max_length=2000)
    arquivada = models.BooleanField(default=False)


class Documento(models.Model):

    usuario_criacao = models.ForeignKey(User, on_delete=models.CASCADE, auto_created=True)

    nome = models.CharField(max_length=200)
    texto = RichTextField(null=True, blank=True)