from django.contrib import admin 
from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'lancamento_cliente', views.LancamentoViewSet, basename='Lancamento')

urlpatterns = [

    path('', include(router.urls)),
    path('dashboard/', views.dashboard, name='dashboard_cliente'),

    path('abastecedor/criar/', views.abastecedor_criar, name='abastecedor_criar'),
    path('abastecedor/listar/', views.abastecedor_listar, name='abastecedor_listar'),
    path('abastecedor/atualizar/<int:abastecedor_id>/', views.abastecedor_atualizar, name='abastecedor_atualizar'),
    #path('abastecedor/excluir/<int:abastecedor_id>/', views.abastecedor_excluir, name='abastecedor_excluir'),

    path('anotacao/criar/', views.anotacao_abastecedor_criar, name='anotacao_abastecedor_criar'),
    path('anotacao/listar/', views.anotacao_abastecedor_listar, name='anotacao_abastecedor_listar'),
    path('anotacao/atualizar/<int:anotacao_id>/', views.anotacao_abastecedor_atualizar, name='anotacao_abastecedor_atualizar'),
    path('anotacao/excluir/<int:anotacao_id>/', views.anotacao_abastecedor_excluir, name='anotacao_abastecedor_excluir'),

    path('anotacao-operario/criar/', views.anotacao_operario_criar, name='anotacao_cliente_operario_criar'),
    path('anotacao-operario/listar/', views.anotacao_operario_listar, name='anotacao_cliente_operario_listar'),
    path('anotacao-operario/atualizar/<int:anotacao_id>/', views.anotacao_operario_atualizar,
         name='anotacao_cliente_operario_atualizar'),
    path('anotacao-operario/excluir/<int:anotacao_id>/', views.anotacao_operario_excluir,
         name='anotacao_cliente_operario_excluir'),

    path('lancamento/', views.lancamento_cliente_listar, name='lancamento_cliente_listar'),
    path('lancamento/criar/', views.lancamento_cliente_criar, name='lancamento_cliente_lcriar'),
    path('lancamento/atualizar/<int:lancamento_id>/', views.lancamento_cliente_atualizar, name='lancamento_cliente_atualizar'),
    path('lancamento/excluir/<int:lancamento_id>/',
         views.lancamento_cliente_excluir, name='lancamento_cliente_excluir'),

    path('dashboard/', views.dashboard, name='dashboard_cliente'),

    path('lavoura/listar/', views.lavoura_listar, name='lavoura_listar'),
    path('lavoura/criar/', views.lavoura_criar, name='lavoura_criar'),
    path('lavoura/detalhe/<int:lavoura_id>/', views.lavoura_detalhe, name='lavoura_detalhe'),
    path('lavoura/atualizar/<int:lavoura_id>/', views.lavoura_atualizar, name='lavoura_atualizar'),
    path('lavoura/excluir/<int:lavoura_id>/', views.lavoura_excluir, name='lavoura_excluir'),

    path('operario/criar/', views.operario_criar, name='operario_criar'),
    path('operario/listar/', views.operario_listar, name='operario_listar'),
    path('operario/atualizar/<int:operario_id>/', views.operario_atualizar, name='operario_atualizar'),
    path('operario/excluir/<int:operario_id>/', views.operario_excluir, name='operario_excluir'),
    path('operario/qrcode/<int:operario_id>/', views.operario_qrcode, name='operario_qrcode'),
    path('operario/menu/<int:operario_id>/', views.operario_menu, name='operario_menu'),

    path('associado/criar/', views.associado_criar, name='associado_criar'),
    path('associado/listar/', views.associado_listar, name='associado_listar'),
    path('associado/atualizar/<int:associado_id>/', views.associado_atualizar, name='associado_atualizar'),
    path('associado/excluir/<int:associado_id>/', views.associado_excluir, name='associado_excluir'),

    path('lancamento/<int:operario_id>/', views.lancamentos_operario, name='lancamentos_operario'),
    path('lancamento/consultar/<int:lancamento_id>/', views.consultar_cancelado, name='consultar_cancelado'),

    path('pagamento/<int:operario_id>/', views.pagamento_lancamento, name='pagamento_lancamento'),
    path('pagamento/cancelar/<int:lancamento_id>/', views.pagamento_cancelar, name='pagamento_cancelar'),
    path('pagamento/desfazer/<int:lancamento_id>/', views.pagamento_desfazer, name='pagamento_desfazer'),

    path('perfil/', views.cliente_perfil_atualizar, name='cliente_perfil_atualizar'),

    path('operario/cracha/<int:operario_id>/', views.operario_cracha, name='operario_cracha'),

    path('anotacao/arquivar/<int:anotacao_id>/', views.anotacao_operario_arquivar,
         name='anotacao_operario_arquivar'),

    path('documento/criar/', views.documento_criar, name='documento_criar'),
    path('documento/listar/', views.documento_listar, name='documento_listar'),
    path('documento/atualizar/<int:documento_id>/', views.documento_atualizar,
         name='documento_atualizar'),
    path('documento/excluir/<int:documento_id>/', views.documento_excluir,
         name='documento_excluir'),

    path('recibo/<int:operario_id>/', views.recibo_listar, name='recibo_listar'),

    path('recibo/imprimir/<int:recibo_id>/', views.recibo_imprimir, name='recibo_imprimir'),
]