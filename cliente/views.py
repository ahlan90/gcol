from itertools import chain

from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.forms import inlineformset_factory, formset_factory, modelformset_factory, RadioSelect
from rest_framework.permissions import IsAuthenticated

from abastecedor.forms import LancamentoForm, PagamentoForm, AnotacaoOperarioForm
from abastecedor.models import Lancamento, Recibo
from api.serializers import LancamentoSerializer, LancamentoListaSerializer
from cliente.filters import LancamentoFilter
from core.forms import SignUpForm, SignUpUpdateForm
from .forms import *
from gerenciamento.forms import ClienteForm, EnderecoClienteForm, UsuarioForm
from .models import *
from gerenciamento.models import EnderecoResidencial
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Sum, F, Count, DecimalField, Value
import json
import time
import datetime
from dateutil.rrule import rrule, MONTHLY
from rest_framework import viewsets
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string
from weasyprint import HTML

@login_required
def dashboard(request, template_name='cliente/dashboard/dashboard.html'):

    try:
        cliente = request.user.usuario_cliente
    except:
        cliente = request.user.associado.cliente_associado

    lancamentos = Lancamento.objects \
        .values('preco_unidade_medida__lavoura__nome') \
        .annotate(producao_total=Sum('credito'), valor_total=Sum('valor')) \
        .filter(preco_unidade_medida__lavoura__cliente_criacao=cliente, cancelada=False).order_by()

    lancamento_filter = LancamentoFilter(request.GET, queryset=lancamentos, cliente=cliente)

    lavouras = [obj.get('preco_unidade_medida__lavoura__nome') for obj in lancamento_filter.qs]
    creditos = [str(obj.get('producao_total')) for obj in lancamento_filter.qs]
    valores = [str(obj.get('valor_total')) for obj in lancamento_filter.qs]

    data = {
        'lavouras': json.dumps(lavouras),
        'creditos': json.dumps(creditos),
        'valores': json.dumps(valores),
        'filter': lancamento_filter
    }
    return render(request, template_name, data)

@login_required
def abastecedor_criar(request, template_name='cliente/abastecedor/abastecedor_criar.html'):

    try:
        cliente = request.user.usuario_cliente
    except:
        cliente = request.user.associado.cliente_associado


    numero_lavoura = len(Lavoura.objects.filter(cliente_criacao=cliente))


    if request.method == "POST":
        form_signup = SignUpForm(request.POST)
        form = AbastecedorForm(request.POST)
        form_contato = ContatoForm(request.POST)
        form_endereco = EnderecoAbastecedorForm(request.POST)
        if form_signup.is_valid() and form.is_valid() and form_endereco.is_valid() and form_contato.is_valid():
            user = form_signup.save(commit=False)
            user.save()

            endereco = form_endereco.save(commit=False)
            endereco.save()

            contato = form_contato.save(commit=False)
            contato.save()

            abastecedor = form.save(commit=False)
            abastecedor.contato_proximo = contato
            abastecedor.endereco_residencial = endereco
            abastecedor.user = user
            abastecedor.cliente_criacao = request.user.usuario_cliente
            abastecedor.save()

            return redirect('abastecedor_listar')
    else:
        form_signup = SignUpForm()
        form = AbastecedorForm()
        form_endereco = EnderecoAbastecedorForm()
        form_contato = ContatoForm()

    data = {
        'form_signup': form_signup,
        'form': form,
        'form_endereco': form_endereco,
        'form_contato': form_contato,
        'menu_abastecedor': 'active',
        'numero_lavoura': numero_lavoura
    }

    return render(request, template_name, data)


@login_required
def abastecedor_listar(request, template_name='cliente/abastecedor/abastecedor_lista.html'):

    try:
        cliente = request.user.usuario_cliente
    except:
        cliente = request.user.associado.cliente_associado


    numero_lavoura = len(Lavoura.objects.filter(cliente_criacao=cliente))

    abastecedores = Abastecedor.objects.filter(cliente_criacao=request.user.usuario_cliente)

    data = {}
    data['abastecedors'] = abastecedores
    data['menu_abastecedor'] = 'active'
    data['numero_lavoura'] = numero_lavoura
    return render(request, template_name, data)


@login_required
def abastecedor_atualizar(request, abastecedor_id, template_name='cliente/abastecedor/abastecedor_atualizar.html'):
    abastecedor = get_object_or_404(Abastecedor, pk=abastecedor_id)

    if request.method == 'POST':
        form_signup = SignUpUpdateForm(request.POST, instance=abastecedor.user)
        form = AbastecedorForm(request.POST, instance=abastecedor)
        form_contato = ContatoForm(request.POST, instance=abastecedor.contato_proximo)
        form_endereco = EnderecoAbastecedorForm(request.POST, instance=abastecedor.endereco_residencial)
        if form.is_valid() and form_endereco.is_valid() and form_contato.is_valid():
            user = form_signup.save()

            endereco = form_endereco.save(commit=False)
            endereco.save()

            contato = form_contato.save(commit=False)
            contato.save()

            abastecedor = form.save(commit=False)
            abastecedor.contato_proximo = contato
            abastecedor.endereco_residencial = endereco
            abastecedor.user = user
            abastecedor.cliente_criacao = request.user.usuario_cliente
            abastecedor.save()

            return redirect('abastecedor_listar')
    else:
        form_signup = SignUpUpdateForm(instance=abastecedor.user)
        form = AbastecedorForm(instance=abastecedor)
        form_contato = ContatoForm(instance=abastecedor.contato_proximo)
        form_endereco = EnderecoAbastecedorForm(instance=abastecedor.endereco_residencial)

    return render(request, template_name, {'form_signup': form_signup, 'form': form, 'form_endereco': form_endereco,
                                           'form_contato': form_contato, 'menu_abastecedor': 'active'})


@login_required
def abastecedor_excluir(request, abastecedor_id):
    abastecedor = get_object_or_404(Abastecedor, pk=abastecedor_id)
    data = dict()
    if request.method == 'POST':
        abastecedor.delete()
        data['form_is_valid'] = True
        abastecedors = Abastecedor.objects.all()
        data['html_abastecedor_list'] = render_to_string('cliente/abastecedor/abastecedor_lista_parcial.html', {
            'abastecedors': abastecedors
        })
    else:
        context = {'abastecedor': abastecedor}
        data['html_form'] = render_to_string('cliente/abastecedor/abastecedor_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def anotacao_abastecedor_criar(request, template_name='cliente/anotacao_abastecedor/anotacao_criar.html'):
    cliente = Cliente.objects.get(user=request.user)

    if request.method == "POST":
        form = AnotacaoAbastecedorForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('anotacao_abastecedor_listar')
    else:
        form = AnotacaoAbastecedorForm(initial={'usuario_criacao': request.user}, cliente=cliente)

    return render(request, template_name, {'form': form, 'menu_cliente_anotacao': 'active'})


@login_required
def anotacao_abastecedor_listar(request, template_name='cliente/anotacao_abastecedor/anotacao_lista.html'):
    anotacoes = Anotacao.objects.filter(usuario_criacao=request.user).filter(operario__isnull=True)
    data = {}
    data['anotacoes'] = anotacoes
    data['menu_cliente_anotacao'] = 'active'

    return render(request, template_name, data)


@login_required
def anotacao_abastecedor_atualizar(request, anotacao_id,
                                   template_name='cliente/anotacao_abastecedor/anotacao_atualizar.html'):
    anotacao = get_object_or_404(Anotacao, pk=anotacao_id)

    if request.method == 'POST':
        form = AnotacaoAbastecedorForm(request.POST, instance=anotacao)
        if form.is_valid():
            form.save()
            return redirect('anotacao_abastecedor_listar')
    else:
        form = AnotacaoAbastecedorForm(instance=anotacao)

    return render(request, template_name,
                  {'form': form, 'menu_cliente_anotacao': 'active'})


@login_required
def anotacao_abastecedor_excluir(request, anotacao_id):
    anotacao = get_object_or_404(Anotacao, pk=anotacao_id)
    data = dict()
    if request.method == 'POST':
        anotacao.delete()
        data['form_is_valid'] = True
        anotacoes = Anotacao.objects.filter(usuario_criacao=request.user).filter(operario__isnull=True)
        data['html_anotacao_list'] = render_to_string('cliente/anotacao_abastecedor/anotacao_lista_parcial.html', {
            'anotacoes': anotacoes
        })
    else:
        context = {'anotacao': anotacao, 'menu_cliente_anotacao': 'active'}
        data['html_form'] = render_to_string('cliente/anotacao_abastecedor/anotacao_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def anotacao_operario_criar(request, template_name='cliente/anotacao_operario/anotacao_criar.html'):
    cliente = Cliente.objects.get(user=request.user)

    if request.method == "POST":
        form = AnotacaoOperarioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('anotacao_cliente_operario_listar')
    else:
        form = AnotacaoOperarioForm(initial={'usuario_criacao': request.user}, cliente=cliente)

    return render(request, template_name, {'form': form, 'menu_cliente_operario_anotacao': 'active'})


@login_required
def anotacao_operario_listar(request, template_name='cliente/anotacao_operario/anotacao_lista.html'):
    anotacoes = Anotacao.objects.filter(usuario_criacao=request.user).filter(operario__isnull=False)

    data = {}
    data['anotacoes'] = anotacoes
    data['menu_cliente_operario_anotacao'] = 'active'

    return render(request, template_name, data)


@login_required
def anotacao_operario_atualizar(request, anotacao_id,
                                template_name='cliente/anotacao_operario/anotacao_atualizar.html'):
    anotacao = get_object_or_404(Anotacao, pk=anotacao_id)

    if request.method == 'POST':
        form = AnotacaoOperarioForm(request.POST, instance=anotacao)
        if form.is_valid():
            form.save()
            return redirect('anotacao_cliente_operario_listar')
    else:
        form = AnotacaoOperarioForm(instance=anotacao)

    return render(request, template_name,
                  {'form': form, 'menu_cliente_operario_anotacao': 'active'})


@login_required
def anotacao_operario_excluir(request, anotacao_id):
    anotacao = get_object_or_404(Anotacao, pk=anotacao_id)
    data = dict()
    if request.method == 'POST':
        anotacao.delete()
        data['form_is_valid'] = True
        anotacoes = Anotacao.objects.filter(usuario_criacao=request.user).filter(operario__isnull=True)
        data['html_anotacao_list'] = render_to_string('cliente/anotacao_operario/anotacao_lista_parcial.html', {
            'anotacoes': anotacoes
        })
    else:
        context = {'anotacao': anotacao, 'menu_cliente_operario_anotacao': 'active'}
        data['html_form'] = render_to_string('cliente/anotacao_operario/anotacao_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def lavoura_criar(request, template_name='cliente/lavoura/lavoura_criar.html'):
    if request.method == "POST":
        form = LavouraForm(request.POST)
        if form.is_valid():
            lavoura = form.save(commit=False)
            lavoura.cliente_criacao = request.user.usuario_cliente
            lavoura.save()
            form.save_m2m()
            return redirect('lavoura_detalhe', lavoura_id=lavoura.pk)
    else:
        form = LavouraForm()

    return render(request, template_name, {'form': form, 'menu_lavoura': 'active'})


@login_required
def lavoura_detalhe(request, lavoura_id, template_name='cliente/lavoura/lavoura_detalhe.html'):
    lavoura = Lavoura.objects.get(pk=lavoura_id)

    PrecoFormSet = inlineformset_factory(Lavoura, PrecoUnidadeMedida, form=PrecoUnidadeMedidaForm, extra=1)

    if request.method == "POST":
        form = LavouraForm(request.POST, instance=lavoura)
        form_endereco = EnderecoLavouraForm(request.POST)
        formset = PrecoFormSet(request.POST, instance=lavoura)
        if form.is_valid() and form_endereco.is_valid() and formset.is_valid():
            lavoura = form.save(commit=False)
            lavoura.save()
            endereco_lavoura = form_endereco.save(commit=False)
            endereco_lavoura.lavoura = lavoura
            endereco_lavoura.save()
            formset.save()
            return redirect('lavoura_listar')
    else:
        form = LavouraForm(instance=lavoura)
        form_endereco = EnderecoLavouraForm()
        formset = PrecoFormSet()

    return render(request, template_name,
                  {'form': form, 'form_endereco': form_endereco, 'formset': formset, 'menu_lavoura': 'active'})


@login_required
def lavoura_listar(request, template_name='cliente/lavoura/lavoura_lista.html'):

    try:
        cliente = request.user.usuario_cliente
    except:
        cliente = request.user.associado.cliente_associado


    lavouras = Lavoura.objects.filter(cliente_criacao=cliente)
    data = {}

    data['lavouras'] = lavouras
    data['menu_lavoura'] = 'active'

    return render(request, template_name, data)


@login_required
def lavoura_atualizar(request, lavoura_id, template_name='cliente/lavoura/lavoura_detalhe_atualizar.html'):
    lavoura = Lavoura.objects.get(pk=lavoura_id)

    PrecoFormSet = inlineformset_factory(Lavoura, PrecoUnidadeMedida, form=PrecoUnidadeMedidaForm, extra=1)

    if request.method == "POST":
        form = LavouraForm(request.POST, instance=lavoura)
        form_endereco = EnderecoLavouraForm(request.POST, instance=lavoura.endereco)
        formset = PrecoFormSet(request.POST, instance=lavoura)
        if form.is_valid() and form_endereco.is_valid() and formset.is_valid():
            lavoura = form.save()
            endereco_lavoura = form_endereco.save(commit=False)
            endereco_lavoura.lavoura = lavoura
            endereco_lavoura.save()
            formset.save()
            return redirect('lavoura_listar')
    else:
        form = LavouraForm(instance=lavoura)
        form_endereco = EnderecoLavouraForm(instance=lavoura.endereco)
        formset = PrecoFormSet(instance=lavoura)

    return render(request, template_name,
                  {'form': form, 'form_endereco': form_endereco, 'formset': formset, 'menu_lavoura': 'active'})


@login_required
def lavoura_excluir(request, lavoura_id):
    lavoura = get_object_or_404(Lavoura, pk=lavoura_id)
    data = dict()
    if request.method == 'POST':
        lavoura.delete()
        data['form_is_valid'] = True
        lavouras = Lavoura.objects.all()
        data['html_lavoura_list'] = render_to_string('cliente/lavoura/recibo_lista_parcial.html', {
            'lavouras': lavouras
        })
    else:
        context = {'lavoura': lavoura}
        data['html_form'] = render_to_string('cliente/lavoura/lavoura_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def operario_criar(request, template_name='cliente/operario/operario_criar.html'):
    if request.method == "POST":
        form = OperarioForm(request.POST)
        form_contato = ContatoForm(request.POST)
        form_endereco = EnderecoOperarioForm(request.POST)
        if form.is_valid() and form_endereco.is_valid() and form_contato.is_valid():
            endereco = form_endereco.save(commit=False)
            endereco.save()

            contato = form_contato.save(commit=False)
            contato.save()

            operario = form.save(commit=False)
            operario.contato_proximo = contato
            operario.endereco_residencial = endereco
            operario.cliente_criacao = request.user.usuario_cliente

            operario.save()

            return redirect('operario_listar')
    else:
        form = OperarioForm(initial={'cliente': request.user.usuario_cliente})
        form_endereco = EnderecoOperarioForm()
        form_contato = ContatoForm(request.POST)

    return render(request, template_name, {'form': form, 'form_endereco': form_endereco, 'form_contato': form_contato,
                                           'menu_operario': 'active'})


@login_required
def operario_listar(request, template_name='cliente/operario/operario_lista.html'):

    try:
        cliente = request.user.usuario_cliente
    except:
        cliente = request.user.associado.cliente_associado

    operarios = Operario.objects.filter(cliente_criacao=cliente)
    data = {}
    data['operarios'] = operarios
    data['menu_operario'] = 'active'
    data['teste'] = '1-1'

    return render(request, template_name, data)


@login_required
def operario_atualizar(request, operario_id, template_name='cliente/operario/operario_atualizar.html'):
    operario = get_object_or_404(Operario, pk=operario_id)

    if request.method == 'POST':
        form = OperarioForm(request.POST, instance=operario)
        form_contato = ContatoForm(request.POST, instance=operario.contato_proximo)
        form_endereco = EnderecoOperarioForm(request.POST, instance=operario.endereco_residencial)
        if form.is_valid() and form_endereco.is_valid() and form_contato.is_valid():
            endereco = form_endereco.save(commit=False)
            endereco.save()

            contato = form_contato.save(commit=False)
            contato.save()

            operario = form.save(commit=False)
            operario.contato_proximo = contato
            operario.endereco_residencial = endereco

            operario.save()

            return redirect('operario_listar')
    else:
        form = OperarioForm(instance=operario)
        form_contato = ContatoForm(instance=operario.contato_proximo)
        form_endereco = EnderecoOperarioForm(instance=operario.endereco_residencial)

    return render(request, template_name, {'form': form, 'form_endereco': form_endereco, 'form_contato': form_contato,
                                           'menu_operario': 'active'})


@login_required
def operario_excluir(request, operario_id):
    operario = get_object_or_404(Operario, pk=operario_id)
    data = dict()
    if request.method == 'POST':
        operario.delete()
        data['form_is_valid'] = True
        operarios = Operario.objects.all()
        data['html_operario_list'] = render_to_string('cliente/operario/operario_lista_parcial.html', {
            'operarios': operarios
        })
    else:
        context = {'operario': operario}
        data['html_form'] = render_to_string('cliente/operario/operario_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def operario_qrcode(request, operario_id):
    operario = get_object_or_404(Operario, pk=operario_id)
    data = dict()

    qrcode = str(operario.cliente_criacao.id) + '-' + str(operario.id)

    context = {
        'operario': operario,
        'qrcode': qrcode
    }
    data['html_form'] = render_to_string('cliente/operario/operario_qrcode.html', context, request=request)

    return JsonResponse(data)


@login_required
def operario_cracha(request, operario_id):
    operario = get_object_or_404(Operario, pk=operario_id)
    data = dict()

    qrcode = str(operario.cliente_criacao.id) + '-' + str(operario.id)

    context = {
        'operario': operario,
        'qrcode': qrcode
    }

    data['html_form'] = render_to_string('cliente/operario/operario_cracha.html', context, request=request)
    #return JsonResponse(data)

    return render(request, 'cliente/operario/operario_cracha.html', context)


@login_required
def operario_menu(request, operario_id, template_name='cliente/operario/operario_menu.html'):

    operario = get_object_or_404(Operario, pk=operario_id)

    data = {
        'operario': operario,
        'menu_operario': 'active'
    }
    return render(request, template_name, data)



@login_required
def associado_criar(request, template_name='cliente/associado/associado_criar.html'):
    if request.method == "POST":
        form = AssociadoForm(request.POST)
        form_signup = SignUpForm(request.POST)
        if form.is_valid() and form_signup.is_valid():
            user = form_signup.save(commit=False)
            user.save()

            associado = form.save(commit=False)
            associado.user = user

            associado.save()
            form.save_m2m()
            return redirect('associado_listar')
    else:
        form = AssociadoForm(initial={'cliente_associado': request.user.usuario_cliente}, cliente=request.user.usuario_cliente)
        form_signup = SignUpForm(request.POST)

    return render(request, template_name, {'form': form, 'form_signup': form_signup, 'menu_associado': 'active'})


@login_required
def associado_listar(request, template_name='cliente/associado/associado_lista.html'):
    associados = Associado.objects.filter(cliente_associado=request.user.usuario_cliente)
    data = {}
    data['associados'] = associados
    data['menu_associado'] = 'active'

    return render(request, template_name, data)


@login_required
def associado_atualizar(request, associado_id, template_name='cliente/associado/associado_atualizar.html'):
    associado = get_object_or_404(Associado, pk=associado_id)

    if request.method == 'POST':
        form = AssociadoForm(request.POST, instance=associado)
        form_signup = SignUpUpdateForm(request.POST, instance=associado.user)
        if form.is_valid() and form_signup.is_valid():
            user = form_signup.save(commit=False)
            user.save()

            associado = form.save(commit=False)
            associado.user = user

            associado.save()
            form.save_m2m()
            return redirect('associado_listar')
    else:
        form = AssociadoForm(instance=associado, cliente=request.user.usuario_cliente)
        form_signup = SignUpUpdateForm(instance=associado.user)

    return render(request, template_name, {'form': form, 'form_signup': form_signup, 'menu_associado': 'active'})


@login_required
def associado_excluir(request, associado_id):

    cliente = request.user.usuario_cliente

    associado = get_object_or_404(Associado, pk=associado_id)
    data = dict()
    if request.method == 'POST':
        associado.delete()
        data['form_is_valid'] = True
        associados = Associado.objects.filter(cliente_associado=cliente)
        data['html_associado_list'] = render_to_string('cliente/associado/associado_lista_parcial.html', {
            'associados': associados
        })
    else:
        context = {'associado': associado}
        data['html_form'] = render_to_string('cliente/associado/associado_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def lancamento_cliente_listar(request, template_name='cliente/lancamento/lancamento_lista.html'):

    # # Montando a lista de meses/ano para renderizar na tela
    # data_primeiro_lancamento = Lancamento.objects.values('data').earliest('data')
    # data_ultimo_lancamento = Lancamento.objects.values('data').latest('data')
    # datas_filtro = [dt for dt in rrule(MONTHLY, dtstart=data_primeiro_lancamento['data'], until=data_ultimo_lancamento['data'])]
    #
    #
    # # Mes e Ano default é o do ultimo lancamento
    # mes = data_ultimo_lancamento['data'].month
    # ano = data_ultimo_lancamento['data'].year
    #
    # data_instancia = data_ultimo_lancamento['data']
    #
    # # Se vier paramentros no get sobrescreve o mes e ano
    # if 'mes' in request.GET and 'ano' in request.GET:
    #     mes = request.GET['mes']
    #     ano = request.GET['ano']
    #     data_instancia = datetime.datetime(year=int(ano), month=int(mes), day=1)
    #
    # lancamentos = Lancamento.objects.filter(abastecedor__cliente_criacao=request.user.usuario_cliente)\
    #     .filter(data__year__gte=ano, data__month__gte=mes, data__year__lte=ano, data__month__lte=mes)


    # lancamentos = Lancamento.objects.filter(abastecedor__cliente_criacao=request.user.usuario_cliente)
    #
    # page = request.GET.get('page', 1)
    #
    # paginator = Paginator(lancamentos, 10)
    # try:
    #     lancamentos = paginator.page(page)
    # except PageNotAnInteger:
    #     lancamentos = paginator.page(1)
    # except EmptyPage:
    #     lancamentos = paginator.page(paginator.num_pages)


    data = {}
    #data['lancamentos'] = lancamentos
    # data['datas_filtro'] = datas_filtro
    # data['data_instancia'] = data_instancia
    data['menu_cliente_lancamento'] = 'active'

    return render(request, template_name, data)


@login_required
def lancamento_cliente_criar(request, template_name='cliente/lancamento/lancamento_criar.html'):

    cliente = Cliente.objects.get(user=request.user)

    if request.method == "POST":
        form = LancamentoClienteForm(request.POST)
        if form.is_valid():
            lancamento = form.save(commit=False)
            lancamento.calcula_valor()
            lancamento.save()
            return redirect('lancamento_cliente_listar')
    else:
        form = LancamentoClienteForm(cliente=cliente)

    return render(request, template_name, {'form': form, 'menu_lancamento': 'active'})


@login_required
def lancamento_cliente_atualizar(request, lancamento_id, template_name='cliente/lancamento/lancamento_atualizar.html'):

    lancamento = get_object_or_404(Lancamento, pk=lancamento_id)

    if request.method == 'POST':
        form = LancamentoClienteForm(request.POST, instance=lancamento)
        if form.is_valid():
            lancamento = form.save(commit=False)
            lancamento.calcula_valor()
            lancamento.save()
            return redirect('lancamento_cliente_listar')
    else:
        form = LancamentoClienteForm(instance=lancamento)

    return render(request, template_name,
                  {'form': form, 'menu_lancamento': 'active'})


@login_required
def lancamento_cliente_excluir(request, lancamento_id):

    lancamento = get_object_or_404(Lancamento, pk=lancamento_id)

    data = dict()

    if request.method == 'POST':
        lancamento.delete()
        messages.success(request, 'Lançamento deletado com sucesso!')
        return redirect('lancamento_cliente_listar')
    else:
        context = {'lancamento': lancamento, 'menu_lancamento': 'active'}
        data['html_form'] = render_to_string('cliente/lancamento/lancamento_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)



@login_required
def lancamentos_operario(request, operario_id, template_name='cliente/operario/pagamento_lista.html'):
    operario = get_object_or_404(Operario, pk=operario_id)
    lancamentos = Lancamento.objects.filter(operario=operario)

    data = {}

    data['operario'] = operario
    data['lancamentos'] = lancamentos
    data['menu_operario'] = 'active'

    return render(request, template_name, data)


# Finalizar esta parte
@login_required
def pagamento_lancamento(request, operario_id):

    operario = Operario.objects.get(pk=operario_id)
    lancamentos = Lancamento.objects.filter(operario=operario_id).filter(pago=False).filter(cancelada=False)
    lancamentos_pago = Lancamento.objects.filter(operario=operario_id).filter(pago=True)
    LancamentoFormset = modelformset_factory(Lancamento, fields=('pago', 'id'), extra=0)
    anotacoes = Anotacao.objects.filter(operario=operario).filter(arquivada=False)
    lancamentos_cancelados = Lancamento.objects.filter(operario=operario_id).filter(cancelada=True)

    producao_total_lavoura = Lancamento.objects.filter(operario=operario_id).annotate(
        lavoura=F('preco_unidade_medida__lavoura__nome')).values('lavoura').annotate(
        total_credito=Sum('credito')).annotate(total_valor=Sum('valor')).annotate(valor_pago=Value(0, output_field=DecimalField())).annotate(
        valor_nao_pago=Value(0, output_field=DecimalField()))

    producao_total_lavoura_paga = Lancamento.objects.filter(operario=operario_id).filter(pago=True).annotate(
        lavoura=F('preco_unidade_medida__lavoura__nome')).values(
        'lavoura').annotate(total_credito=Value(0, output_field=DecimalField())).annotate(total_valor=Value(0, output_field=DecimalField())).annotate(
        valor_pago=Sum('valor')).annotate(valor_nao_pago=Value(0, output_field=DecimalField()))

    producao_total_lavoura_nao_paga = Lancamento.objects.filter(operario=operario_id).filter(pago=False).annotate(
        lavoura=F('preco_unidade_medida__lavoura__nome')).values('lavoura').annotate(total_credito=Value(0, output_field=DecimalField())).annotate(total_valor=Value(0, output_field=DecimalField())).annotate(
        valor_pago=Value(0, output_field=DecimalField())).annotate(valor_nao_pago=Sum('valor'))

    result_list = list(chain(producao_total_lavoura, producao_total_lavoura_paga, producao_total_lavoura_nao_paga))

    recibos = Recibo.objects.filter(lancamento__operario=operario).distinct()

    data = {}
    data['operario'] = operario
    data['anotacoes'] = anotacoes
    data['recibos'] = recibos
    data['menu_operario'] = 'active'
    data['producao_total_lavoura'] = result_list
    data['lancamentos_cancelados'] = lancamentos_cancelados

    if request.method == 'POST':
        formset = LancamentoFormset(request.POST, queryset=lancamentos)
        if formset.is_valid():
            lancamentos = formset.save(commit=False)

            lancamento_recibo = []

            for lancamento in lancamentos:
                if lancamento.pago:
                    lancamento_recibo.append(lancamento)
                else:
                    lancamento.save()

            if len(lancamento_recibo) > 0:
                recibo = Recibo()
                recibo.save()
                valor_total = 0
                for lancamento in lancamento_recibo:
                    lancamento.recibo = recibo
                    valor_total = valor_total + lancamento.valor
                    lancamento.save()
                recibo.valor_total = valor_total
                recibo.save()


            #formset.save()

            formset = LancamentoFormset(queryset=lancamentos)

            data['formset'] = formset
            data['formset_pago'] = lancamentos_pago
            data['messagem'] = 'Pagamento efetuado com sucesso!'

            return redirect('pagamento_lancamento', operario_id=operario_id)
    else:
        formset = LancamentoFormset(queryset=lancamentos)

    data['formset'] = formset
    data['formset_pago'] = lancamentos_pago

    return render(request, 'cliente/pagamento/pagamento_lista.html', data)


def save_pagamento_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            lancamento = form.save(commit=False)
            lancamento.cancelada = True
            lancamento.pago = False
            lancamento.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def pagamento_cancelar(request, lancamento_id):
    lancamento = get_object_or_404(Lancamento, pk=lancamento_id)
    if request.method == 'POST':
        form = LancamentoCancelamentoForm(request.POST, instance=lancamento)
    else:
        form = LancamentoCancelamentoForm(instance=lancamento)
    return save_pagamento_form(request, form, 'cliente/pagamento/pagamento_cancelar.html')

def pagamento_desfazer(request, lancamento_id):

    data = dict()
    lancamento = get_object_or_404(Lancamento, pk=lancamento_id)
    if request.method == 'POST':
        lancamento.pago = False
        lancamento.save()
        return redirect('pagamento_lancamento', operario_id=lancamento.operario.id)
    else:
        context = {'lancamento': lancamento}
        data['html_form'] = render_to_string('cliente/pagamento/pagamento_confirma_desfazer.html', context, request=request)
        return JsonResponse(data)


@login_required
def cliente_perfil_atualizar(request, template_name='cliente/perfil/cliente_perfil_atualizar.html'):
    cliente = Cliente.objects.get(user=request.user)
    usuario = get_object_or_404(User, pk=cliente.user.id)

    if cliente.endereco != None:
        endereco = EnderecoResidencial.objects.get(pk=cliente.endereco.id)
    else:
        endereco = EnderecoResidencial()

    if request.method == 'POST':
        form = ClienteForm(request.POST, instance=cliente)
        form_user = PerfilForm(request.POST, instance=usuario)
        form_endereco = EnderecoClienteForm(request.POST, instance=endereco)
        if form.is_valid() and form_endereco.is_valid() and form_user.is_valid():
            form_user.save()

            endereco_cliente = form_endereco.save(commit=False)
            endereco_cliente.save()

            cliente = form.save(commit=False)
            cliente.endereco = endereco_cliente
            cliente.save()

            return redirect('cliente_listar')
    else:
        form = ClienteForm(instance=cliente)
        form_user = PerfilForm(instance=usuario)
        form_endereco = EnderecoClienteForm(instance=endereco)

    return render(request, template_name,
                  {'form': form, 'form_endereco': form_endereco, 'form_user': form_user,
                   'menu_cliente_perfil_atualizar': 'active'})


@login_required
def anotacao_operario_arquivar(request, anotacao_id):
    anotacao = get_object_or_404(Anotacao, pk=anotacao_id)
    operario = Operario.objects.get(id=anotacao.operario.id)
    data = dict()
    if request.method == 'POST':
        anotacao.arquivada = True
        anotacao.save()
        anotacoes = Anotacao.objects.filter(operario=operario).filter(arquivada=False)
        data['form_is_valid'] = True
        data['html_anotacao_list'] = render_to_string('cliente/pagamento/anotacao_lista_parcial.html', {
            'anotacoes': anotacoes
        })
    else:
        context = {'anotacao': anotacao}
        data['html_form'] = render_to_string('cliente/anotacao_operario/anotacao_confirma_arquivar.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def consultar_cancelado(request, lancamento_id):
    lancamento = get_object_or_404(Lancamento, pk=lancamento_id)
    data = dict()
    context = {'lancamento': lancamento}
    data['html_form'] = render_to_string('cliente/pagamento/lancamento_texto.html', context,
                                         request=request)
    return JsonResponse(data)


class LancamentoViewSet(viewsets.ModelViewSet):

    serializer_class = LancamentoListaSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Lancamento.objects.filter(abastecedor__cliente_criacao=self.request.user.usuario_cliente)


@login_required
def recibo_listar(request, operario_id, template_name='cliente/recibo/recibo_lista.html'):

    operario = get_object_or_404(Operario, pk=operario_id)

    recibos = Recibo.objects.filter(lancamento__operario=operario).distinct()

    data = {}
    data['recibos'] = recibos
    data['menu_abastecedor'] = 'active'
    return render(request, template_name, data)


@login_required
def recibo_imprimir(request, recibo_id):

    recibo = get_object_or_404(Recibo, pk=recibo_id)


    documento = Documento.objects.filter(nome__contains='Recibo').first()

    documento.texto = documento.texto + '<p>R$' + str(recibo.valor_total) + '</p>'

    html_string = render_to_string('cliente/pagamento/recibo_pdf.html', {'texto': documento.texto})

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/mypdf.pdf');

    fs = FileSystemStorage('/tmp')
    with fs.open('mypdf.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="recibo.pdf"'
        return response

    return response



@login_required
def documento_criar(request, template_name='cliente/documento/documento_criar.html'):
    if request.method == "POST":
        form = DocumentoForm(request.POST)
        if form.is_valid():
            documento = form.save(commit=False)
            documento.usuario_criacao = request.user
            documento.save()
            return redirect('documento_listar')
    else:
        form = DocumentoForm()

    return render(request, template_name, {'form': form, 'menu_documento': 'active'})


@login_required
def documento_listar(request, template_name='cliente/documento/documento_lista.html'):

    try:
        cliente = request.user
    except:
        cliente = request.user.associado.user

    documentos = Documento.objects.filter(usuario_criacao=cliente)
    data = {}

    data['documentos'] = documentos
    data['menu_documento'] = 'active'

    return render(request, template_name, data)


@login_required
def documento_atualizar(request, documento_id, template_name='cliente/documento/documento_atualizar.html'):

    documento = Documento.objects.get(pk=documento_id)

    if request.method == "POST":
        form = DocumentoForm(request.POST, instance=documento)
        if form.is_valid():
            form.save()
            return redirect('documento_listar')
    else:
        form = DocumentoForm(instance=documento)
    return render(request, template_name, {'form': form, 'menu_documento': 'active'})


@login_required
def documento_excluir(request, documento_id):
    documento = get_object_or_404(Documento, pk=documento_id)
    data = dict()
    if request.method == 'POST':
        documento.delete()
        data['form_is_valid'] = True
        documentos = Documento.objects.all()
        data['html_documento_list'] = render_to_string('cliente/documento/recibo_lista_parcial.html', {
            'documentos': documentos
        })
    else:
        context = {'documento': documento}
        data['html_form'] = render_to_string('cliente/documento/documento_confirma_delete.html', context, request=request)
    return JsonResponse(data)

