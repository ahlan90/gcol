from django import template

register = template.Library()

"""
Filtros de Template para saber o perfil do usuario
"""
@register.filter(name='is_abastecedor')
def is_abastecedor(user):
    if hasattr(user, 'usuario_abastecedor'):
        return True
    else:
        return False


@register.filter(name='is_cliente')
def is_cliente(user):
    if hasattr(user, 'usuario_cliente'):
        return True
    else:
        return False


@register.filter(name='is_associado')
def is_associado(user):
    if hasattr(user, 'associado'):
        return True
    else:
        return False