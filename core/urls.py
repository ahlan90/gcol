from django.urls import path
from core import views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('inscricao/', views.inscricao, name='inscricao'),
    path('api-token-auth/', views.CustomAuthToken.as_view(), name='token-api'),
]