from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import logout, login, authenticate
from .forms import SignUpForm
from gerenciamento.models import Cliente
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django import template



@login_required
def dashboard(request):
    if hasattr(request.user, 'usuario_cliente'):
        return redirect('dashboard_cliente')
    elif hasattr(request.user, 'associado'):
        return redirect('dashboard_associado')
    elif request.user.is_staff:
        return redirect('dashboard_gerenciamento')
    else:
        return redirect('dashboard_abastecedor')

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})

        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']

        token, created = Token.objects.get_or_create(user=user)

        return Response({
            'client_id': user.usuario_abastecedor.cliente_criacao.pk,
            'token': token.key,
            'user_id': user.usuario_abastecedor.pk,
            'nome': user.first_name + " " + user.last_name,
        })

def inscricao(request):

    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            novo_user = form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            cliente = Cliente()
            cliente.user = novo_user

            cliente.save()

            return redirect('/')
    else:
        form = SignUpForm()

    return render(request, 'registration/inscricao.html', {'form': form})