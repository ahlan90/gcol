from .base import *
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'r458+@0$lr-=l(a7e*9xz^d3_)uzpiv_%(^^k1i*kh12uzsm26'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gcoldb',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

#ROOT_URLCONF = 'gcol.urls'