

/* DATA TABLES DE anotacao */
$(document).ready(function() {
       
    var table = $('#anotacao-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
              {
                "width" : "3%",
              },
               null,
               null,
               { 
                   "width" : "18%",
                   "order" : false,
                   "sortable" : false,
               }
        ]
    } );
   
   
    
});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-anotacao .modal-content").html("");
     $("#modal-anotacao").modal("show");
   },
   success: function (data) {
     $("#modal-anotacao .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#anotacao-table tbody").html(data.html_anotacao_list);
       $("#modal-anotacao").modal("hide");
     }
     else {
       $("#modal-anotacao .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create anotacao
$(".js-create-anotacao").click(loadForm);
$("#modal-anotacao").on("submit", ".js-anotacao-create-form", saveForm);

// Update anotacao
$("#anotacao-table").on("click", ".js-update-anotacao", loadForm);
$("#modal-anotacao").on("submit", ".js-anotacao-update-form", saveForm);

// Delete anotacao
$("#anotacao-table").on("click", ".js-delete-anotacao", loadForm);
$("#modal-anotacao").on("submit", ".js-anotacao-delete-form", saveForm);

});




  