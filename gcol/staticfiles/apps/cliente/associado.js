

/* DATA TABLES DE associado */
$(document).ready(function() {
       
    var table = $('#associado-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
              {
                "width" : "3%",
              },
               null,
               null,
               { 
                   "width" : "18%",
                   "order" : false,
                   "sortable" : false,
               }
        ]
    } );
   
   
    
});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-associado .modal-content").html("");
     $("#modal-associado").modal("show");
   },
   success: function (data) {
     $("#modal-associado .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#associado-table tbody").html(data.html_associado_list);
       $("#modal-associado").modal("hide");
     }
     else {
       $("#modal-associado .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create associado
$(".js-create-associado").click(loadForm);
$("#modal-associado").on("submit", ".js-associado-create-form", saveForm);

// Update associado
$("#associado-table").on("click", ".js-update-associado", loadForm);
$("#modal-associado").on("submit", ".js-associado-update-form", saveForm);

// Delete associado
$("#associado-table").on("click", ".js-delete-associado", loadForm);
$("#modal-associado").on("submit", ".js-associado-delete-form", saveForm);

});




  