var formato = { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' }
$(document).ready(function() {

    //$.fn.dataTable.moment( 'DD/MM/YYYY' );

    var table = $('#dashboard-table').DataTable( {
        // "searching": false,geny
        //  "paging": false,
        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
              {
                "width" : "3%",
              },
              null,
              null,
              null,
              null
        ],
//
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\R$.,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };


            // Total over this page
            pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 3 ).footer() ).html(
                (pageTotal/100).toLocaleString('pt-BR', formato)
            );


            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 4 ).footer() ).html(
                (pageTotal/100).toLocaleString('pt-BR')
            );
        },
//        "initComplete": function () {
//            this.api().column(2).every( function () {
//                var column = this;
//                var select = $('<select class="form-control"  style="font-weight: bold;"><option value="" selected style="font-weight: bold;">Todas Lavouras</option></select>')
//                    .appendTo( $(column.header()).empty() )
//                    .on( 'change', function () {
//                        var val = $.fn.dataTable.util.escapeRegex(
//                            $(this).val()
//                        );
//
//                        column
//                            .search( val ? '^'+val+'$' : '', true, false )
//                            .draw();
//                    } );
//
//                column.data().unique().sort().each( function ( d, j ) {
//                    select.append( '<option value="'+d+'">'+d+'</option>' )
//                } );
//            } );
//
//            this.api().column(4).every( function () {
//                var column = this;
//                var select = $('<select class="form-control"  style="font-weight: bold;"><option value="" selected style="font-weight: bold;">Todas Unidades</option></select>')
//                    .appendTo( $(column.header()).empty() )
//                    .on( 'change', function () {
//                        var val = $.fn.dataTable.util.escapeRegex(
//                            $(this).val()
//                        );
//
//                        column
//                            .search( val ? '^'+val+'$' : '', true, false )
//                            .draw();
//                    } );
//
//                column.data().unique().sort().each( function ( d, j ) {
//                    select.append( '<option value="'+d+'">'+d+'</option>' )
//                } );
//            } );
//        }

    });

});




  