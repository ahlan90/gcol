

/* DATA TABLES DE operario */
$(document).ready(function() {
       
    var table = $('#operario-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
              {
                "width" : "3%",
              },
               null,
               null,
               null,
               { 
                   "width" : "20%",
                   "order" : false,
                   "sortable" : false,
               }
        ],
       "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );


    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
   
   
    
});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-operario .modal-content").html("");
     $("#modal-operario").modal("show");
   },
   success: function (data) {
     $("#modal-operario .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#operario-table tbody").html(data.html_operario_list);
       $("#modal-operario").modal("hide");
     }
     else {
       $("#modal-operario .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create operario
$(".js-create-operario").click(loadForm);
$("#modal-operario").on("submit", ".js-operario-create-form", saveForm);

// Update operario
$("#operario-table").on("click", ".js-update-operario", loadForm);
$("#modal-operario").on("submit", ".js-operario-update-form", saveForm);

// Delete operario
$("#operario-table").on("click", ".js-delete-operario", loadForm);
$("#modal-operario").on("submit", ".js-operario-delete-form", saveForm);

$("#operario-table").on("click", ".js-qrcode-operario", loadForm);

$("#operario-table").on("click", ".js-cracha-operario", loadForm);

});




  