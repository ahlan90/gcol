

/* DATA TABLES DE usuario */
$(document).ready(function() {
       
    var table = $('#usuario-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
              {
                "width" : "3%",
              },
               null,
               null,
               { 
                   "width" : "18%",
                   "order" : false,
                   "sortable" : false,
               }
        ]
    } );
   
   
    
});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-usuario .modal-content").html("");
     $("#modal-usuario").modal("show");
   },
   success: function (data) {
     $("#modal-usuario .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#usuario-table tbody").html(data.html_usuario_list);
       $("#modal-usuario").modal("hide");
     }
     else {
       $("#modal-usuario .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create usuario
$(".js-create-usuario").click(loadForm);
$("#modal-usuario").on("submit", ".js-usuario-create-form", saveForm);

// Update usuario
$("#usuario-table").on("click", ".js-update-usuario", loadForm);
$("#modal-usuario").on("submit", ".js-usuario-update-form", saveForm);

// Delete usuario
$("#usuario-table").on("click", ".js-delete-usuario", loadForm);
$("#modal-usuario").on("submit", ".js-usuario-delete-form", saveForm);

});




  