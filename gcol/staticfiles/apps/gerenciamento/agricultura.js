
/* DATA TABLES DE agricultura */
$(document).ready(function() {
       
    var table = $('#agricultura-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
                {
                   "width" : "5%",
                   "order" : false,
                   "sortable" : false,
               },
               null,
               null,
               null,
               { 
                   "width" : "15%",
                   "order" : false,
                   "sortable" : false,
               }
        ]
    } );
   
   
    
});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-agricultura .modal-content").html("");
     $("#modal-agricultura").modal("show");
   },
   success: function (data) {
     $("#modal-agricultura .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#agricultura-table tbody").html(data.html_agricultura_list);
       $("#modal-agricultura").modal("hide");
     }
     else {
       $("#modal-agricultura .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create agricultura
$(".js-create-agricultura").click(loadForm);
$("#modal-agricultura").on("submit", ".js-agricultura-create-form", saveForm);

// Update agricultura
$("#agricultura-table").on("click", ".js-update-agricultura", loadForm);
$("#modal-agricultura").on("submit", ".js-agricultura-update-form", saveForm);

// Delete agricultura
$("#agricultura-table").on("click", ".js-delete-agricultura", loadForm);
$("#modal-agricultura").on("submit", ".js-agricultura-delete-form", saveForm);

});




  