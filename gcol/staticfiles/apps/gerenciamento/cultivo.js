
/* DATA TABLES DE cultivo */
$(document).ready(function() {
       
    var table = $('#cultivo-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
                {
                   "width" : "5%",
                   "order" : false,
                   "sortable" : false,
               },
               null,
               null,
               null,
               null,
               { 
                   "width" : "7%",
                   "order" : false,
                   "sortable" : false,
               }
        ]
    } );
   
   
    
});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-cultivo .modal-content").html("");
     $("#modal-cultivo").modal("show");
   },
   success: function (data) {
     $("#modal-cultivo .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#cultivo-table tbody").html(data.html_cultivo_list);
       $("#modal-cultivo").modal("hide");
     }
     else {
       $("#modal-cultivo .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create cultivo
$(".js-create-cultivo").click(loadForm);
$("#modal-cultivo").on("submit", ".js-cultivo-create-form", saveForm);

// Update cultivo
$("#cultivo-table").on("click", ".js-update-cultivo", loadForm);
$("#modal-cultivo").on("submit", ".js-cultivo-update-form", saveForm);

// Delete cultivo
$("#cultivo-table").on("click", ".js-delete-cultivo", loadForm);
$("#modal-cultivo").on("submit", ".js-cultivo-delete-form", saveForm);

});




  