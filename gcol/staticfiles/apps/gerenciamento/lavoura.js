
/* DATA TABLES DE lavoura */
$(document).ready(function() {
       
    var table = $('#lavoura-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
              {
                "width" : "3%",
              },
               null,
               null,
               { 
                   "width" : "15%",
                   "order" : false,
                   "sortable" : false,
               }
        ]
    } );
   
   
    
});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-lavoura .modal-content").html("");
     $("#modal-lavoura").modal("show");
   },
   success: function (data) {
     $("#modal-lavoura .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#lavoura-table tbody").html(data.html_lavoura_list);
       $("#modal-lavoura").modal("hide");
     }
     else {
       $("#modal-lavoura .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create lavoura
$(".js-create-lavoura").click(loadForm);
$("#modal-lavoura").on("submit", ".js-lavoura-create-form", saveForm);

// Update lavoura
$("#lavoura-table").on("click", ".js-update-lavoura", loadForm);
$("#modal-lavoura").on("submit", ".js-lavoura-update-form", saveForm);

// Delete lavoura
$("#lavoura-table").on("click", ".js-delete-lavoura", loadForm);
$("#modal-lavoura").on("submit", ".js-lavoura-delete-form", saveForm);

});




  