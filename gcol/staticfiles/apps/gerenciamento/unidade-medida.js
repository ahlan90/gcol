
/* DATA TABLES DE unidade-medida */
$(document).ready(function() {
       
    var table = $('#unidade-medida-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
               {
                "width" : "3%",
               },
               null,
               null,
               null,
               {
                    "width" : "10%",
               },
               { 
                   "width" : "5%",
                   "order" : false,
                   "sortable" : false,
               }
        ]
    } );
   
   
    
});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-unidade-medida .modal-content").html("");
     $("#modal-unidade-medida").modal("show");
   },
   success: function (data) {
     $("#modal-unidade-medida .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#unidade-medida-table tbody").html(data.html_unidade_medida_list);
       $("#modal-unidade-medida").modal("hide");
     }
     else {
       $("#modal-unidade-medida .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create unidade-medida
$(".js-create-unidade-medida").click(loadForm);
$("#modal-unidade-medida").on("submit", ".js-unidade-medida-create-form", saveForm);

// Update unidade-medida
$("#unidade-medida-table").on("click", ".js-update-unidade-medida", loadForm);
$("#modal-unidade-medida").on("submit", ".js-unidade-medida-update-form", saveForm);

// Delete unidade-medida
$("#unidade-medida-table").on("click", ".js-delete-unidade-medida", loadForm);
$("#modal-unidade-medida").on("submit", ".js-unidade-medida-delete-form", saveForm);

});




  