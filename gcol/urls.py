from django.contrib import admin
from django.urls import path, include
from qr_code import urls as qr_code_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls')),
    path('', include('core.urls')),
    path('gerenciamento/', include('gerenciamento.urls')),
    path('cliente/', include('cliente.urls')),
    path('abastecedor/', include('abastecedor.urls')),
    path('associados/', include('associados.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include('api.urls')),
    path('select2/', include('django_select2.urls')),
    path('hijack/', include('hijack.urls', namespace='hijack')),
    path('qr_code/', include(qr_code_urls, namespace="qr_code"))
]
