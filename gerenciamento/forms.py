from django import forms
from django.forms import ModelForm
from .models import *
from django.contrib.auth.models import User



class UsuarioForm(ModelForm):
    first_name = forms.CharField(max_length=30, label='Nome')
    last_name = forms.CharField(max_length=30, label='Sobrenome')
    email = forms.EmailField(max_length=254)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class CultivoForm(ModelForm):
  
    class Meta:
        model = Cultivo
        fields = '__all__'


class ClienteForm(ModelForm):
  
    class Meta:
        model = Cliente
        exclude = ['endereco', 'user', 'cadastro_completo']


class EnderecoClienteForm(ModelForm):
  
    class Meta:
        model = EnderecoResidencial
        fields = '__all__'
        exclude = ['cliente']


class UnidadeMedidaForm(ModelForm):
  
    class Meta:
        model = UnidadeMedida
        fields = '__all__'