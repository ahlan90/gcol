# Generated by Django 2.1.4 on 2019-02-12 18:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0027_auto_20190212_1305'),
        ('abastecedor', '0008_auto_20190204_1646'),
        ('gerenciamento', '0003_auto_20190203_1230'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Agricultura',
            new_name='Cultivo',
        ),
    ]
