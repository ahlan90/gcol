from django.db import models
from django.contrib.auth.models import User

class EnderecoResidencial(models.Model):

    pais = models.CharField(max_length=200, default='Brasil')
    estado = models.CharField(max_length=200, default='Espírito Santo')
    cidade = models.CharField(max_length=200)
    cep = models.CharField(max_length=200)
    bairro = models.CharField(max_length=200)
    logradouro = models.CharField(max_length=200)
    numero = models.CharField(max_length=200)
    complemento = models.CharField(max_length=200, blank=True)



""" TAMBEM CHAMADO COMO PRODUTOR """
class Cliente(models.Model):

    celular = models.CharField(max_length=200, blank=True)
    cpf = models.CharField(max_length=200, blank=True)
    identidade = models.CharField(max_length=200, blank=True)
    endereco = models.OneToOneField(EnderecoResidencial, on_delete=models.CASCADE, null=True)

    cadastro_completo = models.BooleanField(default=False)

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='usuario_cliente')

    inativo = models.BooleanField(default=False)

    def __str__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)


class UnidadeMedida(models.Model):

    medida = models.CharField(max_length=200)
    unidade = models.CharField(max_length=200)
    quantidade_unidade = models.DecimalField(max_digits=6, decimal_places=2)

    inativa = models.BooleanField(default=False)

    def __str__(self):
        return '%s - %s/%s' % (self.medida, self.quantidade_unidade, self.unidade)

    def __unicode__(self):
        return '%s - %s/%s' % (self.medida, self.quantidade_unidade, self.unidade)


class Cultivo(models.Model):

    plantio = models.CharField(max_length=200)
    especie = models.CharField('Espécie', max_length=200)
    tipo = models.CharField(max_length=200)

    inativo = models.BooleanField(default=False)

    def __str__(self):
        return '%s/%s/%s' % (self.plantio, self.especie, self.tipo)