from django.contrib import admin 
from django.urls import path, include
from . import views

urlpatterns = [
    path('dashboard/', views.dashboard, name='dashboard_gerenciamento'),
    path('gerenciamento/relatorio', views.relatorio, name='relatorio'),

    path('unidade-medida/criar/', views.unidade_medida_criar, name='unidade_medida_criar'),
    path('unidade-medida/listar/', views.unidade_medida_listar, name='unidade_medida_listar'),
    path('unidade-medida/atualizar/<int:unidade_medida_id>/', views.unidade_medida_atualizar, name='unidade_medida_atualizar'),
    #path('unidade-medida/excluir/<int:unidade_medida_id>/', views.unidade_medida_excluir, name='unidade_medida_excluir'),

    path('cultivo/criar/', views.cultivo_criar, name='cultivo_criar'),
    path('cultivo/listar/', views.cultivo_listar, name='cultivo_listar'),
    path('cultivo/atualizar/<int:cultivo_id>/', views.cultivo_atualizar, name='cultivo_atualizar'),
    path('cultivo/excluir/<int:cultivo_id>/', views.cultivo_excluir, name='cultivo_excluir'),

    path('cliente/criar/', views.cliente_criar, name='cliente_criar'),
    path('cliente/listar/', views.cliente_listar, name='cliente_listar'),
    path('cliente/atualizar/<int:cliente_id>/', views.cliente_atualizar, name='cliente_atualizar'),
    path('cliente/excluir/<int:cliente_id>/', views.cliente_excluir, name='cliente_excluir'),

    path('abastecedor/listar/', views.abastecedor_gerenciamento_listar, name='abastecedor_gerenciamento_listar'),
    path('abastecedor/atualizar/<int:abastecedor_id>/', views.abastecedor_gerenciamento_atualizar, name='abastecedor_gerenciamento_atualizar'),
]