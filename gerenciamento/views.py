from django.shortcuts import render

from abastecedor.models import Lancamento
from cliente.forms import AbastecedorForm, ContatoForm, EnderecoAbastecedorForm
from cliente.models import Abastecedor
from core.forms import SignUpForm, SignUpUpdateForm
from .forms import *
from .models import *
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.core import serializers
from django.http import HttpResponse, request
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import json


@login_required
def dashboard(request):
    return render(request, 'gerenciamento/dashboard/dashboard.html', {'menu_dashboard_adm': 'active'})


@login_required
def relatorio(request, template_name='gerenciamento/relatorio/relatorio.html'):
    lancamentos = Lancamento.objects.all()

    return render(request, template_name, {'menu_relatorio': 'active', 'lancamentos': lancamentos})


@login_required
def cultivo_listar(request, template_name='gerenciamento/cultivo/cultivo_lista.html'):
    """ Lista das solucoes ja cadastradas ao problema """
    cultivos = Cultivo.objects.all()

    data = {}
    data['cultivos'] = cultivos
    data['menu_cultivo'] = 'active'

    return render(request, template_name, data)


@login_required
def cultivo_excluir(request, cultivo_id):
    cultivo = get_object_or_404(Cultivo, pk=cultivo_id)
    data = dict()
    if request.method == 'POST':
        cultivo.delete()
        data['form_is_valid'] = True
        cultivos = Cultivo.objects.all()
        data['html_cultivo_list'] = render_to_string('gerenciamento/cultivo/cultivo_lista_parcial.html', {
            'cultivos': cultivos
        })
    else:
        context = {'cultivo': cultivo, 'menu_cultivo': 'active'}
        data['html_form'] = render_to_string('gerenciamento/cultivo/cultivo_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def save_cultivo_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            cultivos = Cultivo.objects.all()
            data['html_cultivo_list'] = render_to_string('gerenciamento/cultivo/cultivo_lista_parcial.html', {
                'cultivos': cultivos
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form, 'menu_cultivo': 'active'}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def cultivo_criar(request):
    if request.method == 'POST':
        form = CultivoForm(request.POST)
    else:
        form = CultivoForm()
    return save_cultivo_form(request, form, 'gerenciamento/cultivo/cultivo_modal_criar.html')


@login_required
def cultivo_atualizar(request, cultivo_id):
    cultivo = get_object_or_404(Cultivo, pk=cultivo_id)
    if request.method == 'POST':
        form = CultivoForm(request.POST, instance=cultivo)
    else:
        form = CultivoForm(instance=cultivo)
    return save_cultivo_form(request, form, 'gerenciamento/cultivo/cultivo_modal_atualizar.html')


@login_required
def unidade_medida_listar(request, template_name='gerenciamento/unidade_medida/unidade_medida_lista.html'):
    """ Lista das solucoes ja cadastradas ao problema """
    unidade_medidas = UnidadeMedida.objects.all();

    data = {}
    data['unidade_medidas'] = unidade_medidas
    data['menu_unidade_medida'] = 'active'

    return render(request, template_name, data)


@login_required
def unidade_medida_excluir(request, unidade_medida_id):
    unidade_medida = get_object_or_404(UnidadeMedida, pk=unidade_medida_id)
    data = dict()
    if request.method == 'POST':
        unidade_medida.delete()
        data['form_is_valid'] = True
        unidade_medidas = UnidadeMedida.objects.all()
        data['html_unidade_medida_list'] = render_to_string(
            'gerenciamento/unidade_medida/unidade_medida_lista_parcial.html', {
                'unidade_medidas': unidade_medidas
            })
    else:
        context = {'unidade_medida': unidade_medida, 'menu_unidade_medida': 'active'}
        data['html_form'] = render_to_string('gerenciamento/unidade_medida/unidade_medida_confirma_delete.html',
                                             context, request=request)
    return JsonResponse(data)


@login_required
def save_unidade_medida_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            unidade_medidas = UnidadeMedida.objects.all()
            data['html_unidade_medida_list'] = render_to_string(
                'gerenciamento/unidade_medida/unidade_medida_lista_parcial.html', {
                    'unidade_medidas': unidade_medidas
                })
        else:
            data['form_is_valid'] = False
    context = {'form': form, 'menu_unidade_medida': 'active'}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def unidade_medida_criar(request):
    if request.method == 'POST':
        form = UnidadeMedidaForm(request.POST)
    else:
        form = UnidadeMedidaForm()
    return save_unidade_medida_form(request, form, 'gerenciamento/unidade_medida/unidade_medida_modal_criar.html')


@login_required
def unidade_medida_atualizar(request, unidade_medida_id):
    unidade_medida = get_object_or_404(UnidadeMedida, pk=unidade_medida_id)
    if request.method == 'POST':
        form = UnidadeMedidaForm(request.POST, instance=unidade_medida)
    else:
        form = UnidadeMedidaForm(instance=unidade_medida)
    return save_unidade_medida_form(request, form, 'gerenciamento/unidade_medida/unidade_medida_modal_atualizar.html')


# TODO: Criar opcao do admin poder criar o usuario, problema no FORM (Signupform)
@login_required
def cliente_criar(request, template_name='gerenciamento/cliente/cliente_criar.html'):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        form_cliente = ClienteForm(request.POST)
        form_endereco = EnderecoClienteForm(request.POST)
        if form.is_valid() and form_endereco.is_valid() and form_cliente.is_valid():
            novo_user = form.save()

            endereco_cliente = form_endereco.save(commit=False)
            endereco_cliente.save()

            cliente = form_cliente.save(commit=False)
            cliente.user = novo_user
            cliente.endereco = endereco_cliente
            cliente.save()

            return redirect('cliente_listar')
    else:
        form = SignUpForm()
        form_cliente = ClienteForm()
        form_endereco = EnderecoClienteForm()

    return render(request, template_name, {'form': form, 'form_cliente': form_cliente, 'form_endereco': form_endereco,
                                           'menu_cliente': 'active'})


@login_required
def cliente_listar(request, template_name='gerenciamento/cliente/cliente_lista.html'):
    clientes = Cliente.objects.all();
    data = {}
    data['clientes'] = clientes
    data['menu_cliente'] = 'active'

    return render(request, template_name, data)


@login_required
def cliente_atualizar(request, cliente_id, template_name='gerenciamento/cliente/cliente_atualizar.html'):
    cliente = get_object_or_404(Cliente, pk=cliente_id)
    usuario = get_object_or_404(User, pk=cliente.user.id)

    if cliente.endereco != None:
        endereco = EnderecoResidencial.objects.get(pk=cliente.endereco.id)
    else:
        endereco = EnderecoResidencial()

    if request.method == 'POST':
        form = ClienteForm(request.POST, instance=cliente)
        form_user = UsuarioForm(request.POST, instance=usuario)
        form_endereco = EnderecoClienteForm(request.POST, instance=endereco)
        if form.is_valid() and form_endereco.is_valid() and form_user.is_valid():
            form_user.save()

            endereco_cliente = form_endereco.save(commit=False)
            endereco_cliente.save()

            cliente = form.save(commit=False)
            cliente.endereco = endereco_cliente
            cliente.save()

            return redirect('cliente_listar')
    else:
        form = ClienteForm(instance=cliente)
        form_user = UsuarioForm(instance=usuario)
        form_endereco = EnderecoClienteForm(instance=endereco)

    return render(request, template_name,
                  {'form': form, 'form_endereco': form_endereco, 'form_user': form_user, 'menu_cliente': 'active'})


@login_required
def cliente_excluir(request, cliente_id):
    cliente = get_object_or_404(Cliente, pk=cliente_id)
    data = dict()
    if request.method == 'POST':
        cliente.delete()
        data['form_is_valid'] = True
        clientes = Cliente.objects.all()
        data['html_cliente_list'] = render_to_string('gerenciamento/cliente/cliente_lista_parcial.html', {
            'clientes': clientes
        })
    else:
        context = {'cliente': cliente, 'menu_cliente': 'active'}
        data['html_form'] = render_to_string('gerenciamento/cliente/cliente_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def abastecedor_gerenciamento_listar(request, template_name='gerenciamento/abastecedor/abastecedor_lista.html'):
    abastecedores = Abastecedor.objects.all()
    data = {}
    data['abastecedors'] = abastecedores
    data['menu_abastecedor_gerente'] = 'active'
    return render(request, template_name, data)


@login_required
def abastecedor_gerenciamento_atualizar(request, abastecedor_id,
                                        template_name='gerenciamento/abastecedor/abastecedor_atualizar.html'):
    abastecedor = get_object_or_404(Abastecedor, pk=abastecedor_id)

    if request.method == 'POST':
        form_signup = SignUpUpdateForm(request.POST, instance=abastecedor.user)
        form = AbastecedorForm(request.POST, instance=abastecedor)
        form_contato = ContatoForm(request.POST, instance=abastecedor.contato_proximo)
        form_endereco = EnderecoAbastecedorForm(request.POST, instance=abastecedor.endereco_residencial)
        if form.is_valid() and form_endereco.is_valid() and form_contato.is_valid():
            user = form_signup.save()

            endereco = form_endereco.save(commit=False)
            endereco.save()

            contato = form_contato.save(commit=False)
            contato.save()

            abastecedor = form.save(commit=False)
            abastecedor.contato_proximo = contato
            abastecedor.endereco_residencial = endereco

            abastecedor.save()

            return redirect('abastecedor_gerenciamento_listar')
    else:
        form_signup = SignUpUpdateForm(instance=abastecedor.user)
        form = AbastecedorForm(instance=abastecedor)
        form_contato = ContatoForm(instance=abastecedor.contato_proximo)
        form_endereco = EnderecoAbastecedorForm(instance=abastecedor.endereco_residencial)

    return render(request, template_name, {'form_signup': form_signup, 'form': form, 'form_endereco': form_endereco,
                                           'form_contato': form_contato, 'menu_abastecedor': 'active'})
