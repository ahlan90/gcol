

/* DATA TABLES DE lancamento */
$(document).ready(function() {
       
    var table = $('#lancamento-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
              {
                "width" : "3%",
              },
              {
                "width" : "3%",
              },
               null,
               null,
               null,
               null,
               { 
                   "width" : "12%",
                   "order" : false,
                   "sortable" : false,
               }
        ]
    } );

});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-lancamento .modal-content").html("");
     $("#modal-lancamento").modal("show");
   },
   success: function (data) {
     $("#modal-lancamento .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#lancamento-table tbody").html(data.html_lancamento_list);
       $("#modal-lancamento").modal("hide");
     }
     else {
       $("#modal-lancamento .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create lancamento
$(".js-create-lancamento").click(loadForm);
$("#modal-lancamento").on("submit", ".js-lancamento-create-form", saveForm);

// Update lancamento
$("#lancamento-table").on("click", ".js-update-lancamento", loadForm);
$("#modal-lancamento").on("submit", ".js-lancamento-update-form", saveForm);

// Delete lancamento
$("#lancamento-table").on("click", ".js-delete-lancamento", loadForm);
$("#modal-lancamento").on("submit", ".js-lancamento-delete-form", saveForm);

});




  