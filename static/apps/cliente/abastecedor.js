

/* DATA TABLES DE abastecedor */
$(document).ready(function() {
       
    var table = $('#abastecedor-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
              {
                "width" : "3%",
              },
               null,
               null,
               null,
               { 
                   "width" : "8%",
                   "order" : false,
                   "sortable" : false,
               }
        ]
    } );
   
   
        table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-abastecedor .modal-content").html("");
     $("#modal-abastecedor").modal("show");
   },
   success: function (data) {
     $("#modal-abastecedor .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#abastecedor-table tbody").html(data.html_abastecedor_list);
       $("#modal-abastecedor").modal("hide");
     }
     else {
       $("#modal-abastecedor .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create abastecedor
$(".js-create-abastecedor").click(loadForm);
$("#modal-abastecedor").on("submit", ".js-abastecedor-create-form", saveForm);

// Update abastecedor
$("#abastecedor-table").on("click", ".js-update-abastecedor", loadForm);
$("#modal-abastecedor").on("submit", ".js-abastecedor-update-form", saveForm);

// Delete abastecedor
$("#abastecedor-table").on("click", ".js-delete-abastecedor", loadForm);
$("#modal-abastecedor").on("submit", ".js-abastecedor-delete-form", saveForm);

});




  