/* DATA TABLES DE lancamento */
var formato = { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' }

var total = 0;

$(document).ready(function() {

    //$.fn.dataTable.moment( 'DD/MM/YYYY' );

    var table = $('#pagamento-table').DataTable( {
        // "searching": false,geny
        //  "paging": false,
        "buttons": [ "pdf", "excel" ],
        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "order": [[ 0, "desc" ]],
        "autoWidth": false,
        "columns": [
              {
                "width" : "3%"
              },
              {
                "width" : "3%",
              },
               null,
               null,
               null,
              {
                "width" : "10%",
              },
               {
                "width" : "10%",
              },
              {
                "width" : "10%",
              },
              {
                "width" : "10%",
              },
              {
                "width" : "3%",
              },
        ],

        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\R$.,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };


            // Total over this page
            pageTotal = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 8 ).footer() ).html(
                (pageTotal/100).toLocaleString('pt-BR', formato)
            );


            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 5 ).footer() ).html(
                (pageTotal/100).toLocaleString('pt-BR')
            );
        },
        "initComplete": function () {
            this.api().column(2).every( function () {
                var column = this;
                var select = $('<select class="form-control"  style="font-weight: bold;"><option value="" selected style="font-weight: bold;">Todas Lavouras</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );

            this.api().column(4).every( function () {
                var column = this;
                var select = $('<select class="form-control"  style="font-weight: bold;"><option value="" selected style="font-weight: bold;">Todas Unidades</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }




    });

   table.buttons().container()
        .appendTo( '#pagamento-table_wrapper .col-md-6:eq(0)' );

    var table2 = $('#pagamento-table2').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "order": [[ 0, "desc" ]],
        "columns": [
              {
                "width" : "3%",
              },
              {
                "width" : "3%",
              },
               null,
               null,
               {
                "width" : "10%",
              },
              {
                "width" : "10%",
              },
              {
                "width" : "10%",
              },
              {
                "width" : "20%",
              },
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 6 ).footer() ).html(
                (pageTotal/100).toLocaleString('pt-BR', formato)
            );
        },
                "initComplete": function () {
            this.api().column(2).every( function () {
                var column = this;
                var select = $('<select class="form-control"  style="font-weight: bold;"><option value="" selected style="font-weight: bold;">Todas Lavouras</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }

    });


        var table3 = $('#anotacao-table').DataTable( {

        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "order": [[ 1, "desc" ]],
        "columns": [
              {
                "width" : "3%",
              },
              {
                "width" : "3%",
              },
               null,
              {
                "width" : "5%",
              },
        ],

    });

});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-pagamento .modal-content").html("");
     $("#modal-pagamento").modal("show");
   },
   success: function (data) {
     $("#modal-pagamento .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#modal-pagamento").modal("hide");
     }
     else {
       $("#modal-pagamento .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


var saveFormArquivar = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#anotacao-table tbody").html(data.html_anotacao_list);
       $("#modal-pagamento").modal("hide");
     }
     else {
       $("#modal-pagamento .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};



/* Binding */
// Update lancamento
$("#pagamento-table2").on("click", ".js-cancelar-pagamento", loadForm);
$("#modal-pagamento").on("submit", ".js-pagamento-cancelar-form", saveForm);

// Delete anotacao
$("#anotacao-table").on("click", ".js-arquivar-anotacao", loadForm);
$("#modal-pagamento").on("submit", ".js-anotacao-arquivar-form", saveFormArquivar);

// Delete anotacao
$("#pagamento-table2").on("click", ".js-desfazer-pagamento", loadForm);
$("#modal-pagamento").on("submit", ".js-desfazer-pagamento-form", saveForm);

// Ler texto cancelado
$("#cancelados-table").on("click", ".js-texto-cancelado", loadForm);

$("#marcar-check").on("click", function(){


    event.preventDefault();

    textBtnTotal = $('#valor-total');
    textBtnTotal.text(2);


    var checkboxes = $('input:checkbox');

    checkboxes.prop('checked', true);
    totalPagamento = 0;

    checkboxes.each(function() {

        var valor = $(this).closest('tr').children('.valor-lancamento').text().replace(/[\R$.,]/g, '')*1;
        totalPagamento = totalPagamento + valor;
    })

    textBtnTotal.text((totalPagamento/100).toLocaleString('pt-BR'));
});

$("#desmarcar-check").on("click", function(){


    event.preventDefault();

    textBtnTotal = $('#valor-total');
    textBtnTotal.text(2);

    var checkboxes = $('input:checkbox');

    checkboxes.prop('checked', false);
    totalPagamento = 0;

    checkboxes.each(function() {
        var valor = $(this).closest('tr').children('.valor-lancamento').text().replace(/[\R$.,]/g, '')*1;
    })

    textBtnTotal.text((0).toLocaleString('pt-BR'));
});



});




  