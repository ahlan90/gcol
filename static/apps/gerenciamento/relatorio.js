
/* DATA TABLES DE cliente */
$(document).ready(function() {
       
    var table = $('#lancamento-table').DataTable( {
        "language": {
           "sEmptyTable": "Nenhum registro encontrado",
           "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
           "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
           "sInfoFiltered": "(Filtrados de _MAX_ registros)",
           "sInfoPostFix": "",
           "sInfoThousands": ".",
           "sLengthMenu": "_MENU_ Resultados por página",
           "sLoadingRecords": "Carregando...",
           "sProcessing": "Processando...",
           "sZeroRecords": "Nenhum registro encontrado",
           "sSearch": "Pesquisar: ",
           "oPaginate": {
               "sNext": "Próximo",
               "sPrevious": "Anterior",
               "sFirst": "Primeiro",
               "sLast": "Último"
           },
           "oAria": {
               "sSortAscending": ": Ordenar colunas de forma ascendente",
               "sSortDescending": ": Ordenar colunas de forma descendente"
           }
        },
        "autoWidth": false,
        "columns": [
              null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
        ],
        "initComplete": function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select class="form-control"><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },
    });

    new $.fn.dataTable.Buttons( table, {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
});


$(function () {

/* Functions */

var loadForm = function () {
 var btn = $(this);
 $.ajax({
   url: btn.attr("data-url"),
   type: 'get',
   dataType: 'json',
   beforeSend: function () {
     $("#modal-cliente .modal-content").html("");
     $("#modal-cliente").modal("show");
   },
   success: function (data) {
     $("#modal-cliente .modal-content").html(data.html_form);
   }
 });
};

var saveForm = function () {
 var form = $(this);
 $.ajax({
   url: form.attr("action"),
   data: form.serialize(),
   type: form.attr("method"),
   dataType: 'json',
   success: function (data) {
     if (data.form_is_valid) {
       $("#cliente-table tbody").html(data.html_cliente_list);
       $("#modal-cliente").modal("hide");
     }
     else {
       $("#modal-cliente .modal-content").html(data.html_form);
     }
   }
 });
 return false;
};


/* Binding */

// Create cliente
$(".js-create-cliente").click(loadForm);
$("#modal-cliente").on("submit", ".js-cliente-create-form", saveForm);

// Update cliente
$("#cliente-table").on("click", ".js-update-cliente", loadForm);
$("#modal-cliente").on("submit", ".js-cliente-update-form", saveForm);

// Delete cliente
$("#cliente-table").on("click", ".js-delete-cliente", loadForm);
$("#modal-cliente").on("submit", ".js-cliente-delete-form", saveForm);

});




  